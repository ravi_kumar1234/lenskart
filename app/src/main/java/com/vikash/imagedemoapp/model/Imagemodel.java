package com.vikash.imagedemoapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Vikash Kumar on 8/21/2020
 */
public class Imagemodel {

    @SerializedName("categories")
    @Expose
    private List<String> categories = null;
    @SerializedName("MW0=")
    @Expose
    private MW0 mW0;
    @SerializedName("Mm0=")
    @Expose
    private Mm0 mm0;

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public MW0 getMW0() {
        return mW0;
    }

    public void setMW0(MW0 mW0) {
        this.mW0 = mW0;
    }

    public Mm0 getMm0() {
        return mm0;
    }

    public void setMm0(Mm0 mm0) {
        this.mm0 = mm0;
    }

    public class MW0 {

        @SerializedName("subcategories")
        @Expose
        private List<String> subcategories = null;
        @SerializedName("folder1")
        @Expose
        private List<String> folder1 = null;
        @SerializedName("folder2")
        @Expose
        private List<String> folder2 = null;
        @SerializedName("folder3")
        @Expose
        private List<String> folder3 = null;

        public List<String> getSubcategories() {
            return subcategories;
        }

        public void setSubcategories(List<String> subcategories) {
            this.subcategories = subcategories;
        }

        public List<String> getFolder1() {
            return folder1;
        }

        public void setFolder1(List<String> folder1) {
            this.folder1 = folder1;
        }

        public List<String> getFolder2() {
            return folder2;
        }

        public void setFolder2(List<String> folder2) {
            this.folder2 = folder2;
        }

        public List<String> getFolder3() {
            return folder3;
        }

        public void setFolder3(List<String> folder3) {
            this.folder3 = folder3;
        }

    }

    public class Mm0 {

        @SerializedName("subcategories")
        @Expose
        private List<String> subcategories = null;
        @SerializedName("folder1")
        @Expose
        private List<String> folder1 = null;
        @SerializedName("folder2")
        @Expose
        private List<String> folder2 = null;
        @SerializedName("folder3")
        @Expose
        private List<String> folder3 = null;

        public List<String> getSubcategories() {
            return subcategories;
        }

        public void setSubcategories(List<String> subcategories) {
            this.subcategories = subcategories;
        }
        public List<String> getFolder1() {
            return folder1;
        }

        public void setFolder1(List<String> folder1) {
            this.folder1 = folder1;
        }

        public List<String> getFolder2() {
            return folder2;
        }

        public void setFolder2(List<String> folder2) {
            this.folder2 = folder2;
        }

        public List<String> getFolder3() {
            return folder3;
        }

        public void setFolder3(List<String> folder3) {
            this.folder3 = folder3;
        }
    }
}
