package com.vikash.imagedemoapp.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vikash.imagedemoapp.model.Imagemodel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
/**
 * Created by Vikash Kumar on 1/27/2020
 */
public class PreferenceManager {
    Context context;
    private static PreferenceManager instance = null;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    public PreferenceManager(Context context) {
        this.context =context;
    }

    public static PreferenceManager getInstance(Context context) {
        if (instance == null) {
            instance = new PreferenceManager(context);
            sharedPreferences = context.getSharedPreferences("info", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return instance;
    }
    public void setExpertResumeURL(String expertResumeURL){
        sharedPreferences = context.getSharedPreferences("expert_resume_url", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_resume_url",expertResumeURL).apply();
    }
    public String getExpertResumeURL(){
        sharedPreferences = context.getSharedPreferences("expert_resume_url", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_resume_url","");
    }

    public void setExpertCertURL(String expertCertURL){
        sharedPreferences = context.getSharedPreferences("expert_cert_url", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_cert_url",expertCertURL).apply();
    }
    public String getExpertCertURL(){
        sharedPreferences = context.getSharedPreferences("expert_cert_url", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_cert_url","");
    }

    public void setExpertProfilePic(String expertProfilePic){
        sharedPreferences = context.getSharedPreferences("expert_profile_pic", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_profile_pic",expertProfilePic).apply();
    }
    public String getExpertProfilePic(){
        sharedPreferences = context.getSharedPreferences("expert_profile_pic", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_profile_pic","");
    }

    public void setExpertCoverPic(String expertCoverPic){
        sharedPreferences = context.getSharedPreferences("expert_cover_pic", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_cover_pic",expertCoverPic).apply();
    }
    public String getExpertCoverPic(){
        sharedPreferences = context.getSharedPreferences("expert_cover_pic", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_cover_pic","");
    }

    public void setExpertDocNum(String expertDocNum){
        sharedPreferences = context.getSharedPreferences("expert_doc_num", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_doc_num",expertDocNum).apply();
    }
    public String getExpertDocNum(){
        sharedPreferences = context.getSharedPreferences("expert_doc_num", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_doc_num","");
    }

    public void setExpertDocType(String expertDocType){
        sharedPreferences = context.getSharedPreferences("expert_doc_type", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_doc_type",expertDocType).apply();
    }
    public String getExpertDocType(){
        sharedPreferences = context.getSharedPreferences("expert_doc_type", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_doc_type","");
    }

    public void setExpertSubcategories(String expertSubcategories){
        sharedPreferences = context.getSharedPreferences("expert_subcategories", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_subcategories",expertSubcategories).apply();
    }
    public String getExpertSubcategories(){
        sharedPreferences = context.getSharedPreferences("expert_subcategories", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_subcategories","");
    }

    public void setExpertDescription(String expertDescription){
        sharedPreferences = context.getSharedPreferences("expert_description", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_description",expertDescription).apply();
    }
    public String getExpertDescription(){
        sharedPreferences = context.getSharedPreferences("expert_description", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_description","");
    }

    public void setExpertNation(String expertNation){
        sharedPreferences = context.getSharedPreferences("expert_nation", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_nation",expertNation).apply();
    }
    public String getExpertNation(){
        sharedPreferences = context.getSharedPreferences("expert_nation", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_nation","");
    }

    public void setExpertGender(String expertG){
        sharedPreferences = context.getSharedPreferences("expert_gender", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_gender",expertG).apply();
    }
    public String getExpertGender(){
        sharedPreferences = context.getSharedPreferences("expert_gender", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_gender","");
    }

    public void setExpertDob(String expertDob){
        sharedPreferences = context.getSharedPreferences("expert_dob", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_dob",expertDob).apply();
    }
    public String getExpertDob(){
        sharedPreferences = context.getSharedPreferences("expert_dob", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_dob","");
    }

    public void setExpertEmail(String expertEmail){
        sharedPreferences = context.getSharedPreferences("expert_email", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_email",expertEmail).apply();
    }
    public String getExpertEmail(){
        sharedPreferences = context.getSharedPreferences("expert_email", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_email","");
    }

    public void setExpertMobile(String expertMobile){
        sharedPreferences = context.getSharedPreferences("expert_mobile", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_mobile",expertMobile).apply();
    }
    public String getExpertMobile(){
        sharedPreferences = context.getSharedPreferences("expert_mobile", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_mobile","");
    }

    public void setExpertfirstName(String expertfirstName){
        sharedPreferences = context.getSharedPreferences("expert_firstName", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_firstName",expertfirstName).apply();
    }
    public String getExpertfirstName(){
        sharedPreferences = context.getSharedPreferences("expert_firstName", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_firstName","");
        
    }
    
    public void setExpertlastName(String expertlastName){
        sharedPreferences = context.getSharedPreferences("expert_lastName", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_lastName",expertlastName).apply();
    }
    public String getExpertlastName(){
        sharedPreferences = context.getSharedPreferences("expert_lastName", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_lastName","");
    }

    public void setExpertName(String expertName){
        sharedPreferences = context.getSharedPreferences("expert_name", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("expert_name",expertName).apply();
    }
    public String getExpertName(){
        sharedPreferences = context.getSharedPreferences("expert_name", Context.MODE_PRIVATE);
        return sharedPreferences.getString("expert_name","");
    }
    public void setExpertId(Integer expertId){
        sharedPreferences = context.getSharedPreferences("expert_id", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putInt("expert_id",expertId).apply();
    }
    public Integer getExpertId(){
        sharedPreferences = context.getSharedPreferences("expert_id", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("expert_id",-1);
    }

    public void setUserId(String number){
        sharedPreferences = context.getSharedPreferences("id", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("id",number).apply();
    }
    public String getUserId(){
        sharedPreferences = context.getSharedPreferences("id", Context.MODE_PRIVATE);
        return sharedPreferences.getString("id","");
    }
    public void setPushToken(String pushToken){
        sharedPreferences = context.getSharedPreferences("push_token", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("push_token",pushToken).apply();
    }
    public String getPushToken(){
        sharedPreferences = context.getSharedPreferences("push_token", Context.MODE_PRIVATE);
        return sharedPreferences.getString("push_token","");
    }

    public void setIsFirstTime(boolean isFirstTime){
        sharedPreferences = context.getSharedPreferences("isFirstTime", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean("isFirstTime",isFirstTime).apply();
    }
    public Boolean getIsFirstTime(){
        sharedPreferences = context.getSharedPreferences("isFirstTime", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("isFirstTime",true);
    }

    public String getUserEmail(){
        sharedPreferences = context.getSharedPreferences("email", Context.MODE_PRIVATE);
        return sharedPreferences.getString("email","");
    }

    public void setUserEmail(String email){
        sharedPreferences = context.getSharedPreferences("email", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("email",email).apply();
    }

    public String getDescription(){
        sharedPreferences = context.getSharedPreferences("description", Context.MODE_PRIVATE);
        return sharedPreferences.getString("description","");
    }
    public void setDescription(String description){
        sharedPreferences = context.getSharedPreferences("description", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("description",description).apply();
    }
    public void setPassword(String password){
        sharedPreferences = context.getSharedPreferences("password", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("password",password).apply();
    }
    public String getPassword(){
        sharedPreferences = context.getSharedPreferences("password", Context.MODE_PRIVATE);
        return sharedPreferences.getString("password","");
    }
    public void setPermission(boolean b){
        sharedPreferences  = context.getSharedPreferences("permission",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("permission",b).apply();
    }
    public boolean getPermission(){
        sharedPreferences = context.getSharedPreferences("permission",Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("permission",false);
    }

    public String getGalleryUrl(){
        sharedPreferences = context.getSharedPreferences("gallery", Context.MODE_PRIVATE);
        return sharedPreferences.getString("gallery","");
    }

    public void setGalleryUrl(String url){
        sharedPreferences = context.getSharedPreferences("gallery", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("gallery",url).apply();
    }


    public void setProfileUrl(String url){
        sharedPreferences = context.getSharedPreferences("profile", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("profile",url).apply();
    }
    public String getProfileUrl(){
        sharedPreferences = context.getSharedPreferences("profile", Context.MODE_PRIVATE);
        return sharedPreferences.getString("profile","");
    }

    public String getUserName(){
        sharedPreferences = context.getSharedPreferences("username", Context.MODE_PRIVATE);
        return sharedPreferences.getString("username","");
    }
    public void setUserName(String name){
        sharedPreferences = context.getSharedPreferences("username", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("username",name).apply();
    }

    public String getName(){
        sharedPreferences = context.getSharedPreferences("name", Context.MODE_PRIVATE);
        return sharedPreferences.getString("name","");
    }
    public void setName(String name){
        sharedPreferences = context.getSharedPreferences("name", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("name",name).apply();
    }

    public String getGender(){
        sharedPreferences = context.getSharedPreferences("gender", Context.MODE_PRIVATE);
        return sharedPreferences.getString("gender","");
    }
    public void setGender(String gender){
        sharedPreferences = context.getSharedPreferences("gender", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("gender",gender).apply();
    }

    public String getDob(){
        sharedPreferences = context.getSharedPreferences("dob", Context.MODE_PRIVATE);
        return sharedPreferences.getString("dob","");
    }
    public void setDob(String dob){
        sharedPreferences = context.getSharedPreferences("dob", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("dob",dob).apply();
    }

    public void setToken(String token){
        sharedPreferences  = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token",token).apply();
    }
    public String getToken(){
        sharedPreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        return sharedPreferences.getString("token","");
    }
    public void setFirebaseToken(String token){
        sharedPreferences  = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token",token).apply();
    }
    public String getFirebaseToken(){
        sharedPreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        return sharedPreferences.getString("token","");
    }

    public void setUserLoggedin(boolean b){
        sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("login",b).apply();
    }
    public boolean getUserLoggedIn(){
        sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("login",false);
    }
    public void setLatitude(String lat){
        sharedPreferences = context.getSharedPreferences("lat",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("lat",lat).apply();
    }
    public void setLongitude(String lng){
        sharedPreferences = context.getSharedPreferences("lng",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("lng",lng).apply();
    }
    public String getLatitude(){
        sharedPreferences = context.getSharedPreferences("lat",Context.MODE_PRIVATE);
        return sharedPreferences.getString("lat","");
    }
    public String getLongitude(){
        sharedPreferences = context.getSharedPreferences("lng",Context.MODE_PRIVATE);
        return sharedPreferences.getString("lng","");
    }

    public String getFromClock(){
        sharedPreferences = context.getSharedPreferences("fromClock",Context.MODE_PRIVATE);
        return sharedPreferences.getString("fromClock","");
    }
    public void setFromClock(String fromClock){
        sharedPreferences = context.getSharedPreferences("fromClock",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("fromClock",fromClock).apply();
    }

    public String getMobile(){
        sharedPreferences = context.getSharedPreferences("mobile",Context.MODE_PRIVATE);
        return sharedPreferences.getString("mobile","");
    }
    public void setMobile(String mobile){
        sharedPreferences = context.getSharedPreferences("mobile",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("mobile",mobile).apply();
    }

    public String getWebsite(){
        sharedPreferences = context.getSharedPreferences("website",Context.MODE_PRIVATE);
        return sharedPreferences.getString("website","");
    }
    public void setWebsite(String website){
        sharedPreferences = context.getSharedPreferences("website",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("website",website).apply();
    }


    public boolean getUpdateLocation(){
        sharedPreferences = context.getSharedPreferences("location",Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("location",false);
    }
    public void setUpdateLocation(boolean location){
        sharedPreferences = context.getSharedPreferences("location",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean("location",location).apply();
    }
    public int getSearchRadius(){
        sharedPreferences = context.getSharedPreferences("radius",Context.MODE_PRIVATE);
        return sharedPreferences.getInt("radius",100);
    }
    public void setSearchRadius(int radius){
        sharedPreferences = context.getSharedPreferences("radius",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putInt("radius",radius).apply();
    }

    public void setHideProfile(int bool){
        sharedPreferences = context.getSharedPreferences("hide_profile",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putInt("hide_profile",bool).apply();
    }
    public int getHideProfile(){
        sharedPreferences = context.getSharedPreferences("hide_profile",Context.MODE_PRIVATE);
        return sharedPreferences.getInt("hide_profile",0);
    }

    public void setStatus(String status){
        sharedPreferences = context.getSharedPreferences("availabilty_status",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("availabilty_status",status).apply();
    }
    public String getStatus(){
        sharedPreferences = context.getSharedPreferences("availabilty_status",Context.MODE_PRIVATE);
        return sharedPreferences.getString("availabilty_status","1");
    }

    public void setIsCustomer(String isCustomer){
        sharedPreferences = context.getSharedPreferences("is_customer",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("is_customer",isCustomer).apply();
    }
    public String getIsCustomer(){
        sharedPreferences = context.getSharedPreferences("is_customer",Context.MODE_PRIVATE);
        return sharedPreferences.getString("is_customer","0");
    }

    public void setListShowHide(boolean b){
        sharedPreferences = context.getSharedPreferences("list", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("list",b).apply();
    }
    public boolean getListShowHide(){
        sharedPreferences = context.getSharedPreferences("list", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("list",false);
    }

    public void setnotification(String value){
        sharedPreferences = context.getSharedPreferences("noti", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("noti", value).apply();
    }
    public String getNotification(){
        sharedPreferences = context.getSharedPreferences("noti", Context.MODE_PRIVATE);
        return sharedPreferences.getString("noti","false");
    }

    public void setLanguage(String language){
        sharedPreferences = context.getSharedPreferences("language", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("language", language).apply();
    }
    public String getLanguage(){
        sharedPreferences = context.getSharedPreferences("language", Context.MODE_PRIVATE);
        return sharedPreferences.getString("language","English");
    }

    public void setIsDataRefreshed(String mode){
        sharedPreferences = context.getSharedPreferences("mode", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("mode", mode).apply();
    }
    public String getMode(){
        sharedPreferences = context.getSharedPreferences("mode", Context.MODE_PRIVATE);
        return sharedPreferences.getString("mode","1");
    }
    
    // temporary fix 
    public void setDistances(String id){
        sharedPreferences = context.getSharedPreferences("dis", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("dis", id).apply();
    }
    public String getDistances(){
        sharedPreferences = context.getSharedPreferences("dis", Context.MODE_PRIVATE);
        return sharedPreferences.getString("dis","");
    }

    public boolean clearAllPreferences(){
        sharedPreferences = context.getSharedPreferences("current_query_id", Context.MODE_PRIVATE);
        return sharedPreferences.edit().clear().commit();
    }
    public void setImageModel(Object imagemodel){
        sharedPreferences  = context.getSharedPreferences("Image", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(imagemodel);
        editor.putString("imagemodel",json).apply();
    }
    /*public Imagemodel getImageModel(){
        sharedPreferences = context.getSharedPreferences("Image", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("imagemodel","");
        Object data =  gson.fromJson(json, Object.class);
        return data;
    }*/
    public void setDistance(ArrayList<String> distance){
        sharedPreferences  = context.getSharedPreferences("distance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(distance);
        editor.putString("distance",json).apply();
    }
    public ArrayList<String> getDistance(){
        sharedPreferences = context.getSharedPreferences("distance", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("distance","");
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }

    public HashMap<String,ArrayList<String>> getFolder(String id){
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = context.getSharedPreferences("folder", Context.MODE_PRIVATE);
        java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        String images= sharedPreferences.getString(id, "");
        HashMap<String,ArrayList<String>> gethashmap = gson.fromJson(images, type);
        return  gethashmap;
    }

    public void setFolderData(HashMap<String, ArrayList<String>> jsonMap) {
        String jsonString = new Gson().toJson(jsonMap);
        SharedPreferences sharedPreferences = context.getSharedPreferences("folder", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("folder", jsonString);
        editor.apply();
    }
    public HashMap<String, ArrayList<String>> getFolderData(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("folder", Context.MODE_PRIVATE);
        String defValue = new Gson().toJson(new HashMap<String, ArrayList<String>>());
        String json=sharedPreferences.getString("folder",defValue);
        TypeToken<HashMap<String,ArrayList<String>>> token = new TypeToken<HashMap<String,ArrayList<String>>>() {};
        HashMap<String,ArrayList<String>> retrievedMap=new Gson().fromJson(json,token.getType());
        return retrievedMap;
    }
    public void setHashmapImage(HashMap<String,HashMap<String,ArrayList<Bitmap>>> hashImage) {
        String jsonString = new Gson().toJson(hashImage);
        SharedPreferences sharedPreferences = context.getSharedPreferences("hashImage", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("hashImage", jsonString);
        editor.apply();
    }
    public HashMap<String,HashMap<String,ArrayList<Bitmap>>> getHashMapImage(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("hashImage", Context.MODE_PRIVATE);
        String defValue = new Gson().toJson(new HashMap<String,HashMap<String,ArrayList<Bitmap>>>());
        String json=sharedPreferences.getString("hashImage",defValue);
        TypeToken<HashMap<String,HashMap<String,ArrayList<Bitmap>>>> token = new TypeToken<HashMap<String,HashMap<String,ArrayList<Bitmap>>>>() {};
        HashMap<String,HashMap<String,ArrayList<Bitmap>>> retrievedMap=new Gson().fromJson(json,token.getType());
        return retrievedMap;
    }
}
