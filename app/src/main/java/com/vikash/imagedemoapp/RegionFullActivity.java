package com.vikash.imagedemoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.vikash.imagedemoapp.common.Common;
import com.vikash.imagedemoapp.common.PreferenceManager;
import java.util.ArrayList;

public class RegionFullActivity extends AppCompatActivity {
    private ImageView imageView;
    private ArrayList<Integer> imageList;
    int pos=0;
    private Button buttonNext,buttonBack;
    private String screen="0";
    private PreferenceManager preferenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Common.fullScreencall(getWindow(), this);
        Common.hideKeyboard(this);
        setContentView(R.layout.activity_region_full);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        screen = getIntent().getStringExtra("screen");
        preferenceManager = new PreferenceManager(this);
        imageView = findViewById(R.id.imageView);
        imageList = new ArrayList<Integer>();

        pos = getIntent().getIntExtra("pos",0);

        if (screen.equals("0") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_bengali);
            imageList.add(R.drawable._2_equity_chart_5ft36_bengali);
            imageList.add(R.drawable._3_equity_chart_5ft24_bengali);
            imageList.add(R.drawable._4_equity_chart_5ft18_bengali);
            imageList.add(R.drawable._5_equity_chart_5ft12_bengali);
            imageList.add(R.drawable._6_equity_chart_5ft9_bengali);
            imageList.add(R.drawable._7_equity_chart_5ft6_bengali);
        } else if (screen.equals("1") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_gujrati);
            imageList.add(R.drawable._2_equity_chart_5ft36_gujrati);
            imageList.add(R.drawable._3_equity_chart_5ft24_gujrati);
            imageList.add(R.drawable._4_equity_chart_5ft18_gujrati);
            imageList.add(R.drawable._5_equity_chart_5ft12_gujrati);
            imageList.add(R.drawable._6_equity_chart_5ft9_gujrati);
            imageList.add(R.drawable._7_equity_chart_5ft6_gujrati);
        }
        else if (screen.equals("2") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_kannada);
            imageList.add(R.drawable._2_equity_chart_5ft36_kannada);
            imageList.add(R.drawable._3_equity_chart_5ft24_kannada);
            imageList.add(R.drawable._4_equity_chart_5ft18_kannada);
            imageList.add(R.drawable._5_equity_chart_5ft12_kannada);
            imageList.add(R.drawable._6_equity_chart_5ft9_kannada);
            imageList.add(R.drawable._7_equity_chart_5ft6_kannada);
        }
        else if (screen.equals("3") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_malyalam);
            imageList.add(R.drawable._2_equity_chart_5ft36_malyalam);
            imageList.add(R.drawable._3_equity_chart_5ft24_malyalam);
            imageList.add(R.drawable._4_equity_chart_5ft18_malyalam);
            imageList.add(R.drawable._5_equity_chart_5ft12_malyalam);
            imageList.add(R.drawable._6_equity_chart_5ft9_malyalam);
            imageList.add(R.drawable._7_equity_chart_5ft6_malyalam);
        }
        else if (screen.equals("4") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_oodiya);
            imageList.add(R.drawable._2_equity_chart_5ft36_oodiya);
            imageList.add(R.drawable._3_equity_chart_5ft24_oodiya);
            imageList.add(R.drawable._4_equity_chart_5ft18_oodiya);
            imageList.add(R.drawable._5_equity_chart_5ft12_oodiya);
            imageList.add(R.drawable._6_equity_chart_5ft9_oodiya);
            imageList.add(R.drawable._7_equity_chart_5ft6_oodiya);
        }
        else if (screen.equals("5") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_punjabi);
            imageList.add(R.drawable._2_equity_chart_5ft36_punjabi);
            imageList.add(R.drawable._3_equity_chart_5ft24_punjabi);
            imageList.add(R.drawable._4_equity_chart_5ft18_punjabi);
            imageList.add(R.drawable._5_equity_chart_5ft12_punjabi);
            imageList.add(R.drawable._6_equity_chart_5ft9_punjabi);
            imageList.add(R.drawable._7_equity_chart_5ft6_punjabi);
        }
        else if (screen.equals("6") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_tamil);
            imageList.add(R.drawable._2_equity_chart_5ft36_tamil);
            imageList.add(R.drawable._3_equity_chart_5ft24_tamil);
            imageList.add(R.drawable._4_equity_chart_5ft18_tamil);
            imageList.add(R.drawable._5_equity_chart_5ft12_tamil);
            imageList.add(R.drawable._6_equity_chart_5ft9_tamil);
            imageList.add(R.drawable._7_equity_chart_5ft6_tamil);
        }
        else if (screen.equals("7") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_telugu);
            imageList.add(R.drawable._2_equity_chart_5ft36_telugu);
            imageList.add(R.drawable._3_equity_chart_5ft24_telugu);
            imageList.add(R.drawable._4_equity_chart_5ft18_telugu);
            imageList.add(R.drawable._5_equity_chart_5ft12_telugu);
            imageList.add(R.drawable._6_equity_chart_5ft9_telugu);
            imageList.add(R.drawable._7_equity_chart_5ft6_telugu);
        }

        if (screen.equals("0") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_bengali);
            imageList.add(R.drawable._2_equity_chart_6ft36_bengali);
            imageList.add(R.drawable._3_equity_chart_6ft24_bengali);
            imageList.add(R.drawable._4_equity_chart_6ft18_bengali);
            imageList.add(R.drawable._5_equity_chart_6ft12_bengali);
            imageList.add(R.drawable._6_equity_chart_6ft9_bengali);
            imageList.add(R.drawable._7_equity_chart_6ft6_bengali);
        } else if (screen.equals("1") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_gujrati);
            imageList.add(R.drawable._2_equity_chart_6ft36_gujrati);
            imageList.add(R.drawable._3_equity_chart_6ft24_gujrati);
            imageList.add(R.drawable._4_equity_chart_6ft18_gujrati);
            imageList.add(R.drawable._5_equity_chart_6ft12_gujrati);
            imageList.add(R.drawable._6_equity_chart_6ft9_gujrati);
            imageList.add(R.drawable._7_equity_chart_6ft6_gujrati);
        }
        else if (screen.equals("2") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_kannada);
            imageList.add(R.drawable._2_equity_chart_6ft36_kannada);
            imageList.add(R.drawable._3_equity_chart_6ft24_kannada);
            imageList.add(R.drawable._4_equity_chart_6ft18_kannada);
            imageList.add(R.drawable._5_equity_chart_6ft12_kannada);
            imageList.add(R.drawable._6_equity_chart_6ft9_kannada);
            imageList.add(R.drawable._7_equity_chart_6ft6_kannada);

        }
        else if (screen.equals("3") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_malyalam);
            imageList.add(R.drawable._2_equity_chart_6ft36_malyalam);
            imageList.add(R.drawable._3_equity_chart_6ft24_malyalam);
            imageList.add(R.drawable._4_equity_chart_6ft18_malyalam);
            imageList.add(R.drawable._5_equity_chart_6ft12_malyalam);
            imageList.add(R.drawable._6_equity_chart_6ft9_malyalam);
            imageList.add(R.drawable._7_equity_chart_6ft6_malyalam);
        }
        else if (screen.equals("4") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_oodiya);
            imageList.add(R.drawable._2_equity_chart_6ft36_oodiya);
            imageList.add(R.drawable._3_equity_chart_6ft24_oodiya);
            imageList.add(R.drawable._4_equity_chart_6ft18_oodiya);
            imageList.add(R.drawable._5_equity_chart_6ft12_oodiya);
            imageList.add(R.drawable._6_equity_chart_6ft9_oodiya);
            imageList.add(R.drawable._7_equity_chart_6ft6_oodiya);
        }
        else if (screen.equals("5") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_punjabi);
            imageList.add(R.drawable._2_equity_chart_6ft36_punjabi);
            imageList.add(R.drawable._3_equity_chart_6ft24_punjabi);
            imageList.add(R.drawable._4_equity_chart_6ft18_punjabi);
            imageList.add(R.drawable._5_equity_chart_6ft12_punjabi);
            imageList.add(R.drawable._6_equity_chart_6ft9_punjabi);
            imageList.add(R.drawable._7_equity_chart_6ft6_punjabi);
        }
        else if (screen.equals("6") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_tamil);
            imageList.add(R.drawable._2_equity_chart_6ft36_tamil);
            imageList.add(R.drawable._3_equity_chart_6ft24_tamil);
            imageList.add(R.drawable._4_equity_chart_6ft18_tamil);
            imageList.add(R.drawable._5_equity_chart_6ft12_tamil);
            imageList.add(R.drawable._6_equity_chart_6ft9_tamil);
            imageList.add(R.drawable._7_equity_chart_6ft6_tamil);
        }
        else if (screen.equals("7") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_telugu);
            imageList.add(R.drawable._2_equity_chart_6ft36_telugu);
            imageList.add(R.drawable._3_equity_chart_6ft24_telugu);
            imageList.add(R.drawable._4_equity_chart_6ft18_telugu);
            imageList.add(R.drawable._5_equity_chart_6ft12_telugu);
            imageList.add(R.drawable._6_equity_chart_6ft9_telugu);
            imageList.add(R.drawable._7_equity_chart_6ft6_telugu);
        }
        if (screen.equals("0") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_bengali);
            imageList.add(R.drawable._2_equity_chart_8ft36_bengali);
            imageList.add(R.drawable._3_equity_chart_8ft24_bengali);
            imageList.add(R.drawable._4_equity_chart_8ft18_bengali);
            imageList.add(R.drawable._5_equity_chart_8ft12_bengali);
            imageList.add(R.drawable._6_equity_chart_8ft9_bengali);
            imageList.add(R.drawable._7_equity_chart_8ft6_bengali);
        } else if (screen.equals("1") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_gujrati);
            imageList.add(R.drawable._2_equity_chart_8ft36_gujrati);
            imageList.add(R.drawable._3_equity_chart_8ft24_gujrati);
            imageList.add(R.drawable._4_equity_chart_8ft18_gujrati);
            imageList.add(R.drawable._5_equity_chart_8ft12_gujrati);
            imageList.add(R.drawable._6_equity_chart_8ft9_gujrati);
            imageList.add(R.drawable._7_equity_chart_8ft6_gujrati);
        }
        else if (screen.equals("2") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_kannada);
            imageList.add(R.drawable._2_equity_chart_8ft36_kannada);
            imageList.add(R.drawable._3_equity_chart_8ft24_kannada);
            imageList.add(R.drawable._4_equity_chart_8ft18_kannada);
            imageList.add(R.drawable._5_equity_chart_8ft12_kannada);
            imageList.add(R.drawable._6_equity_chart_8ft9_kannada);
            imageList.add(R.drawable._7_equity_chart_8ft6_kannada);
        }
        else if (screen.equals("3") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_malyalam);
            imageList.add(R.drawable._2_equity_chart_8ft36_malyalam);
            imageList.add(R.drawable._3_equity_chart_8ft24_malyalam);
            imageList.add(R.drawable._4_equity_chart_8ft18_malyalam);
            imageList.add(R.drawable._5_equity_chart_8ft12_malyalam);
            imageList.add(R.drawable._6_equity_chart_8ft9_malyalam);
            imageList.add(R.drawable._7_equity_chart_8ft6_malyalam);
        }
        else if (screen.equals("4") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_oodiya);
            imageList.add(R.drawable._2_equity_chart_8ft36_oodiya);
            imageList.add(R.drawable._3_equity_chart_8ft24_oodiya);
            imageList.add(R.drawable._4_equity_chart_8ft18_oodiya);
            imageList.add(R.drawable._5_equity_chart_8ft12_oodiya);
            imageList.add(R.drawable._6_equity_chart_8ft9_oodiya);
            imageList.add(R.drawable._7_equity_chart_8ft6_oodiya);
        }
        else if (screen.equals("5") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_punjabi);
            imageList.add(R.drawable._2_equity_chart_8ft36_punjabi);
            imageList.add(R.drawable._3_equity_chart_8ft24_punjabi);
            imageList.add(R.drawable._4_equity_chart_8ft18_punjabi);
            imageList.add(R.drawable._5_equity_chart_8ft12_punjabi);
            imageList.add(R.drawable._6_equity_chart_8ft9_punjabi);
            imageList.add(R.drawable._7_equity_chart_8ft6_punjabi);
        }
        else if (screen.equals("6") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_tamil);
            imageList.add(R.drawable._2_equity_chart_8ft36_tamil);
            imageList.add(R.drawable._3_equity_chart_8ft24_tamil);
            imageList.add(R.drawable._4_equity_chart_8ft18_tamil);
            imageList.add(R.drawable._5_equity_chart_8ft12_tamil);
            imageList.add(R.drawable._6_equity_chart_8ft9_tamil);
            imageList.add(R.drawable._7_equity_chart_8ft6_tamil);
        }
        else if (screen.equals("7") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_telugu);
            imageList.add(R.drawable._2_equity_chart_8ft36_telugu);
            imageList.add(R.drawable._3_equity_chart_8ft24_telugu);
            imageList.add(R.drawable._4_equity_chart_8ft18_telugu);
            imageList.add(R.drawable._5_equity_chart_8ft12_telugu);
            imageList.add(R.drawable._6_equity_chart_8ft9_telugu);
            imageList.add(R.drawable._7_equity_chart_8ft6_telugu);
        }

        if (screen.equals("0") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_bengali);
            imageList.add(R.drawable._2_equity_chart_10ft36_bengali);
            imageList.add(R.drawable._3_equity_chart_10ft24_bengali);
            imageList.add(R.drawable._4_equity_chart_10ft18_bengali);
            imageList.add(R.drawable._5_equity_chart_10ft12_bengali);
            imageList.add(R.drawable._6_equity_chart_10ft9_bengali);
            imageList.add(R.drawable._7_equity_chart_10ft6_bengali);
        } else if (screen.equals("1") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_gujrati);
            imageList.add(R.drawable._2_equity_chart_10ft36_gujrati);
            imageList.add(R.drawable._3_equity_chart_10ft24_gujrati);
            imageList.add(R.drawable._4_equity_chart_10ft18_gujrati);
            imageList.add(R.drawable._5_equity_chart_10ft12_gujrati);
            imageList.add(R.drawable._6_equity_chart_10ft9_gujrati);
            imageList.add(R.drawable._7_equity_chart_10ft6_gujrati);
        }
        else if (screen.equals("2") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_kannada);
            imageList.add(R.drawable._2_equity_chart_10ft36_kannada);
            imageList.add(R.drawable._3_equity_chart_10ft24_kannada);
            imageList.add(R.drawable._4_equity_chart_10ft18_kannada);
            imageList.add(R.drawable._5_equity_chart_10ft12_kannada);
            imageList.add(R.drawable._6_equity_chart_10ft9_kannada);
            imageList.add(R.drawable._7_equity_chart_10ft6_kannada);
        }
        else if (screen.equals("3") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_malyalam);
            imageList.add(R.drawable._2_equity_chart_10ft36_malyalam);
            imageList.add(R.drawable._3_equity_chart_10ft24_malyalam);
            imageList.add(R.drawable._4_equity_chart_10ft18_malyalam);
            imageList.add(R.drawable._5_equity_chart_10ft12_malyalam);
            imageList.add(R.drawable._6_equity_chart_10ft9_malyalam);
            imageList.add(R.drawable._7_equity_chart_10ft6_malyalam);
        }
        else if (screen.equals("4") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_oodiya);
            imageList.add(R.drawable._2_equity_chart_10ft36_oodiya);
            imageList.add(R.drawable._3_equity_chart_10ft24_oodiya);
            imageList.add(R.drawable._4_equity_chart_10ft18_oodiya);
            imageList.add(R.drawable._5_equity_chart_10ft12_oodiya);
            imageList.add(R.drawable._6_equity_chart_10ft9_oodiya);
            imageList.add(R.drawable._7_equity_chart_10ft6_oodiya);
        }
        else if (screen.equals("5") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_punjabi);
            imageList.add(R.drawable._2_equity_chart_10ft36_punjabi);
            imageList.add(R.drawable._3_equity_chart_10ft24_punjabi);
            imageList.add(R.drawable._4_equity_chart_10ft18_punjabi);
            imageList.add(R.drawable._5_equity_chart_10ft12_punjabi);
            imageList.add(R.drawable._6_equity_chart_10ft9_punjabi);
            imageList.add(R.drawable._7_equity_chart_10ft6_punjabi);
        }
        else if (screen.equals("6") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_tamil);
            imageList.add(R.drawable._2_equity_chart_10ft36_tamil);
            imageList.add(R.drawable._3_equity_chart_10ft24_tamil);
            imageList.add(R.drawable._4_equity_chart_10ft18_tamil);
            imageList.add(R.drawable._5_equity_chart_10ft12_tamil);
            imageList.add(R.drawable._6_equity_chart_10ft9_tamil);
            imageList.add(R.drawable._7_equity_chart_10ft6_tamil);
        }
        else if (screen.equals("7") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_telugu);
            imageList.add(R.drawable._2_equity_chart_10ft36_telugu);
            imageList.add(R.drawable._3_equity_chart_10ft24_telugu);
            imageList.add(R.drawable._4_equity_chart_10ft18_telugu);
            imageList.add(R.drawable._5_equity_chart_10ft12_telugu);
            imageList.add(R.drawable._6_equity_chart_10ft9_telugu);
            imageList.add(R.drawable._7_equity_chart_10ft6_telugu);
        }
        fullScreenImage(pos);
    }

    private void fullScreenImage(int pos) {
        try{
            Glide.with(this).load(imageList.get(pos)).into(imageView);
        }catch (Exception e){}
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if(imageList.size()>pos+1){
                    pos++;
                    try{
                        imageView.setImageResource(imageList.get(pos));
                    }catch (Exception e){}
                }else{
//                    Toast.makeText(this, "This is last image", Toast.LENGTH_SHORT).show();
                }
                break;
            case KeyEvent.KEYCODE_DPAD_UP_RIGHT:
                if(imageList.size()>pos+1){
                    pos++;
                    try{
                        imageView.setImageResource(imageList.get(pos));
                    }catch (Exception e){}
                }else{
//                    Toast.makeText(this, "This is last image", Toast.LENGTH_SHORT).show();
                }
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if(imageList.size()>=0 && pos>0){
                    // fullScreenImage(pos--);
                    pos--;
                    try{
                        imageView.setImageResource(imageList.get(pos));
                        //  Glide.with(getApplicationContext()).load(imageList.get(pos)).into(imageView);
                    }catch (Exception e){}

                }else{
                    Intent in=new Intent(getApplicationContext(),RegionalActivity.class);
                    startActivity(in);
//                    Toast.makeText(this, "This is first image", Toast.LENGTH_SHORT).show();
                }
                break;
            case KeyEvent.KEYCODE_SOFT_LEFT:
                if(imageList.size()>=0 && pos>0){
                    // fullScreenImage(pos--);
                    pos--;
                    try{
                        imageView.setImageResource(imageList.get(pos));
                        //Glide.with(getApplicationContext()).load(imageList.get(pos)).into(imageView);
                    }catch (Exception e){}

                }else{
                    Intent in=new Intent(getApplicationContext(),RegionalActivity.class);
                    startActivity(in);
//                    Toast.makeText(this, "This is first image", Toast.LENGTH_SHORT).show();
                }
                return true;
           /* case KeyEvent.KEYCODE_ENTER:
                if(imageList.size()>=0){
                    fullScreenImage(0);
                }
                break;*/
            case KeyEvent.KEYCODE_0:
                if(imageList.size()>=0){
                    // fullScreenImage(0);
                    Intent intent = new Intent(RegionFullActivity.this, RegionalActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("folder",10);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
                break;
            case KeyEvent.KEYCODE_1:
                if(imageList.size()>=0){
                    // fullScreenImage(0);
                    Intent intent = new Intent(RegionFullActivity.this, RegionalActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("folder", 1);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
                break;
            case KeyEvent.KEYCODE_2:
                Toast.makeText(getApplicationContext(),"Sorry Item not found for this number",Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_3:
                Toast.makeText(getApplicationContext(),"Sorry Item not found for this number",Toast.LENGTH_SHORT).show();
                break;

            case KeyEvent.KEYCODE_4:
                Toast.makeText(getApplicationContext(),"Sorry Item not found for this number",Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_5:
                Toast.makeText(getApplicationContext(),"Sorry Item not found for this number",Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_6:
                Toast.makeText(getApplicationContext(),"Sorry Item not found for this number",Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_7:
              Toast.makeText(getApplicationContext(),"Sorry Item not found for this number",Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_8:
                Toast.makeText(getApplicationContext(),"Sorry Item not found for this number",Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_9:
                Toast.makeText(getApplicationContext(),"Sorry Item not found for this number",Toast.LENGTH_SHORT).show();
                break;

            case KeyEvent.KEYCODE_BACK:
                Intent in=new Intent(RegionFullActivity.this,RegionalActivity.class);
                startActivity(in);
                finish();
                break;
        }
        return true;
    }
}