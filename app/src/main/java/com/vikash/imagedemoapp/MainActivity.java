package com.vikash.imagedemoapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.vikash.imagedemoapp.common.Common;
import com.vikash.imagedemoapp.common.PreferenceManager;
import com.vikash.imagedemoapp.screen1.HomeScreen;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ImageView img1, img2, img3, img4, img5, img6, img7, img8, img9,img10,img11, imgSetting;
    private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9,tv10,tv11;
    private RecyclerView recyclerview;
    private PreferenceManager preferenceManager;
    private String distance = "";
    private LinearLayout layout3,layout_setting;
    //Ravi Changes
    private LinearLayout firstLL,secondLL,thirdLL,fourLL,fiveLL,sixLL,sevenLL,eightLL,nineLL,tenLL,elevenLL,settingsLL;
    private ArrayList<Integer> imageList;
    private ArrayList<Integer> iconcount;
    int count=1;
    private ImageView settingsIV;
    private String screen="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Common.fullScreencall(getWindow(), this);
        Common.hideKeyboard(this);
        setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        preferenceManager = new PreferenceManager(this);
        firstLL=findViewById(R.id.firstLL);
        secondLL=findViewById(R.id.secondLL);
        thirdLL=findViewById(R.id.thirdLL);
        fourLL=findViewById(R.id.fourLL);
        fiveLL=findViewById(R.id.fiveLL);
        sixLL=findViewById(R.id.sixLL);
        sevenLL=findViewById(R.id.sevenLL);
        eightLL=findViewById(R.id.eightLL);
        nineLL=findViewById(R.id.nineLL);
        tenLL=findViewById(R.id.tenLL);
        elevenLL=findViewById(R.id.elevenLL);
        settingsLL=findViewById(R.id.settingsLL);
        //settingsIV=findViewById(R.id.settingsIV);

        distance = preferenceManager.getDistances() + "ft";
        initializeViews();
        if(getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            int folderId = extras.getInt("folder");
            if(folderId==10){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "0"));
            }
            else if(folderId==1){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "1"));
            }
            else if(folderId==2){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "2"));
            }
            else if(folderId==3){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "3"));
            }
            else if(folderId==4){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "4"));
            }
            else if(folderId==5){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "5"));
            }
            else if(folderId==6){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "6"));
            }
            else if(folderId==7){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "7"));
            }
            else if(folderId==8){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "8"));
            }
            else if(folderId==9){
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "9"));
            }
        }
        initializeClicks();
        setIcons();
        setData();
        calculateSize();
        firstLL.setBackgroundResource(R.drawable.round_corner_select_bg);
       // Log.e("itemcount", String.valueOf(count));
    }
    private void calculateSize() {
        if (preferenceManager.getDistances().equals("5")){
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitychart_english_rev005ft);
            imageList.add(R.drawable._2equitychart_english_rev005ft60);
            imageList.add(R.drawable._3equitychart_english_rev005ft36);
            imageList.add(R.drawable._4equitychart_english_rev005ft24);
            imageList.add(R.drawable._5equitychart_english_rev005ft18);
            imageList.add(R.drawable._6equitychart_english_rev005ft12);
            imageList.add(R.drawable._7equitychart_english_rev005ft9);
            imageList.add(R.drawable._8equitychart_english_rev005ft6);
            imageList.add(R.drawable._9equitychart_english_rev005ft5);
            imageList.add(R.drawable._10equitychart_english_rev005ft);
            imageList.add(R.drawable._11equitychart_english_rev005ft60);
            imageList.add(R.drawable._12equitychart_english_rev005ft36);
            imageList.add(R.drawable._13equitychart_english_rev005ft24);
            imageList.add(R.drawable._14equitychart_english_rev005ft18);
            imageList.add(R.drawable._15equitychart_english_rev005ft12);
            imageList.add(R.drawable._16equitychart_english_rev005ft9);
            imageList.add(R.drawable._17equitychart_english_rev005ft6);
            imageList.add(R.drawable._18equitychart_english_rev005ft5);
            tv1.setText("Alphabets " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
//            imageList.add(Common.folderImage.get(1));
            imageList.add(R.drawable._1landoltc_5ftsingle660);
            imageList.add(R.drawable._2landoltc_5ftsingle636);
            imageList.add(R.drawable._3landoltc_5ftsingle624);
            imageList.add(R.drawable._4landoltc_5ftsingle618);
            imageList.add(R.drawable._5landoltc_5ftsingle612);
            imageList.add(R.drawable._6landoltc_5ftsingle69);
            imageList.add(R.drawable._7landoltc_5ftsingle66);
            imageList.add(R.drawable._8landoltc_5ftsingle65);
            tv2.setText("Landolt C " + distance+ " ("+imageList.size()+")");

        }  if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschar5ft200);
            imageList.add(R.drawable._2dotschart5ft120);
            imageList.add(R.drawable._3dotschart5ft80);
            imageList.add(R.drawable._4dotschart5f60);
            imageList.add(R.drawable._5dotschart5f40);
            imageList.add(R.drawable._6dotschart5f30);
            imageList.add(R.drawable._7dotschart5f20);
            imageList.add(R.drawable._8dotschart5f17);
            tv3.setText("Dots " + distance+ " ("+imageList.size()+")");

        }  if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
//            imageList.add(Common.folderImage.get(3));
            imageList.add(R.drawable._1_duochrome5ft200);
            imageList.add(R.drawable._2_duochrome5ft120);
            imageList.add(R.drawable._3_duochrome5ft80);
            imageList.add(R.drawable._4_duochrome5ft60);
            imageList.add(R.drawable._5_duochrome5ft40);
            imageList.add(R.drawable._6_duochrome5ft30);
            imageList.add(R.drawable._7_duochrome5ft20);
            imageList.add(R.drawable._8_duochrome5ft17);
            tv4.setText("Duochrome " + distance+ " ("+imageList.size()+")");

        }  if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome5ft200);
            imageList.add(R.drawable._2duochrome5ft120);
            imageList.add(R.drawable._3duochrome5ft80);
            imageList.add(R.drawable._4duochrome5f60);
            imageList.add(R.drawable._5duochrome5ft40);
            imageList.add(R.drawable._6duochrome5ft30);
            imageList.add(R.drawable._7duochrome5ft20);
            imageList.add(R.drawable._8duochrome5ft17);
            tv5.setText("Duo Numeric " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("5")){
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartlogmar5ft);
            tv6.setText("Logmar " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("5")){
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev015ft);
            imageList.add(R.drawable._2numberrev005ft60);
            imageList.add(R.drawable._3numberrev005ft36);
            imageList.add(R.drawable._4numberrev5ft24);
            imageList.add(R.drawable._5numberrev5ft18);
            imageList.add(R.drawable._6numberrev5ft12);
            imageList.add(R.drawable._7numberrev5ft9);
            imageList.add(R.drawable._8numberrev005ft6);
            imageList.add(R.drawable._9numberrev005ft5);
            imageList.add(R.drawable._10numberrev015ft);
            imageList.add(R.drawable._11numberrev005ft60);
            imageList.add(R.drawable._12numberrev005ft36);
            imageList.add(R.drawable._13numberrev5ft24);
            imageList.add(R.drawable._14numberrev5ft18);
            imageList.add(R.drawable._15numberrev5ft12);
            imageList.add(R.drawable._16numberrev5ft9);
            imageList.add(R.drawable._17numberrev005ft6);
            imageList.add(R.drawable._18numberrev005ft5);
            tv7.setText("Numerics " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitycharthindi5ft60);
            imageList.add(R.drawable._2equitycharthindi5ft36);
            imageList.add(R.drawable._3equitycharthindi5ft24);
            imageList.add(R.drawable._4equitycharthindi5ft18);
            imageList.add(R.drawable._5equitycharthindi5ft12);
            imageList.add(R.drawable._6equitycharthindi5ft9);
            imageList.add(R.drawable._7equitycharthindi5ft6);
            imageList.add(R.drawable._8equitycharthindi5ft5);
            tv8.setText("Regional " + distance+ " ("+imageList.size()+")");

        } if ( preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1echartrev005ftrev01);
            imageList.add(R.drawable._2echartrev005ftrev60);
            imageList.add(R.drawable._3echartrev005ftrev36);
            imageList.add(R.drawable._4echartrev005ftrev24);
            imageList.add(R.drawable._5echartrev005ftrev18);
            imageList.add(R.drawable._6echartrev005ftrev12);
            imageList.add(R.drawable._7echartrev005ftrev9);
            imageList.add(R.drawable._8echartrev005ftrev6);
            imageList.add(R.drawable._9echartrev005ftrev5);
            tv9.setText("Tumbline E  " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome5fttumbe200);
            imageList.add(R.drawable._2_duochrome5fttumbe120);
            imageList.add(R.drawable._3_duochrome5fttumbe80);
            imageList.add(R.drawable._4_duochrome5fttumbe60);
            imageList.add(R.drawable._5_duochrome5fttumbe40);
            imageList.add(R.drawable._6_duochrome5fttumbe30);
            imageList.add(R.drawable._7_duochrome5fttumbe20);
            imageList.add(R.drawable._8_duochrome5fttumbe17);
            tv10.setText("Duo Tumbline E " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.isihara_1_5);
            imageList.add(R.drawable.isihara_2_5);
            imageList.add(R.drawable.isihara_3_5);
            imageList.add(R.drawable.isihara_4_5);
            imageList.add(R.drawable.isihara_5_5);
            imageList.add(R.drawable.isihara_6_5);
            imageList.add(R.drawable.isihara_7_5);
            imageList.add(R.drawable.isihara_8_5);
            imageList.add(R.drawable.isihara_9_5);
            imageList.add(R.drawable.isihara_10_5);
            imageList.add(R.drawable.isihara_11_5);
            tv11.setText("Isihara " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equity_chartenglishrev_6ft);
            imageList.add(R.drawable._2equity_chartenglishrev_6ft);
            imageList.add(R.drawable._3equity_chartenglishrev_6ft);
            imageList.add(R.drawable._4equity_chartenglishrev_6ft);
            imageList.add(R.drawable._5equity_chartenglishrev_6ft);
            imageList.add(R.drawable._6equity_chartenglishrev_6ft);
            imageList.add(R.drawable._7equity_chartenglishrev_6ft);
            imageList.add(R.drawable._8equity_chartenglishrev_6ft);
            imageList.add(R.drawable._9equity_chartenglishrev_6ft);
            imageList.add(R.drawable._10equity_chartenglishrev_6ft);
            imageList.add(R.drawable._11equity_chartenglishrev_6ft);
            imageList.add(R.drawable._12equity_chartenglishrev_6ft);
            imageList.add(R.drawable._13equity_chartenglishrev_6ft);
            imageList.add(R.drawable._14equity_chartenglishrev_6ft);
            imageList.add(R.drawable._15equity_chartenglishrev_6ft);
            imageList.add(R.drawable._16equity_chartenglishrev_6ft);
            imageList.add(R.drawable._17equity_chartenglishrev_6ft);
            imageList.add(R.drawable._18equity_chartenglishrev_6ft);
            tv1.setText("Alphabets " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._7landoltcsingle_6ft);
            imageList.add(R.drawable._6landoltcsingle_6ft);
            imageList.add(R.drawable._5landoltcsingle_6ft);
            imageList.add(R.drawable._4landoltcsingle_6ft);
            imageList.add(R.drawable._3landoltcsingle_6ft);
            imageList.add(R.drawable._2landoltcsingle_6ft);
            imageList.add(R.drawable._1landoltcsingle_6ft);
            imageList.add(R.drawable._8landoltcsingle_6ft);
            tv2.setText("Landolt C " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschart_6ft200);
            imageList.add(R.drawable._2dotschart_6ft200);
            imageList.add(R.drawable._3dotschart_6ft200);
            imageList.add(R.drawable._4dotschart_6ft200);
            imageList.add(R.drawable._5dotschart_6ft40);
            imageList.add(R.drawable._6dotschart_6ft30);
            imageList.add(R.drawable._7dotschart_6ft20);
            imageList.add(R.drawable._8dotschart_6ft17);
            tv3.setText("Dots " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome_6ft200);
            imageList.add(R.drawable._2duochrome_6ft);
            imageList.add(R.drawable._3duochrome_6ft);
            imageList.add(R.drawable._4duochrome_6ft);
            imageList.add(R.drawable._5duochrome_6ft);
            imageList.add(R.drawable._6duochrome_6ft);
            imageList.add(R.drawable._7duochrome_6ft);
            imageList.add(R.drawable._8duochrome_6ft);
            tv4.setText("Duochrome  " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome_numeric_6_ft);
            imageList.add(R.drawable._2duochrome_numeric_6_ft);
            imageList.add(R.drawable._3duochrome_numeric_6_ft);
            imageList.add(R.drawable._4duochrome_numeric_6_ft);
            imageList.add(R.drawable._5duochrome_numeric_6_ft);
            imageList.add(R.drawable._6duochrome_numeric_6_ft);
            imageList.add(R.drawable._7duochrome_numeric_6_ft);
            imageList.add(R.drawable._8duochrome_numeric_6_ft);
            tv5.setText("Duo Numeric " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")){
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartlogma_6ft);
            tv6.setText("Logmar " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev_6ft);
            imageList.add(R.drawable._2numberrev_6ft);
            imageList.add(R.drawable._3numberrev_6ft);
            imageList.add(R.drawable._4numberrev_6ft);
            imageList.add(R.drawable._5numberrev_6ft);
            imageList.add(R.drawable._6numberrev_6ft);
            imageList.add(R.drawable._7numberrev_6ft);
            imageList.add(R.drawable._8numberrev_6ft);
            imageList.add(R.drawable._9numberrev_6ft);
            imageList.add(R.drawable._10numberrev_6ft);
            imageList.add(R.drawable._11numberrev_6ft);
            imageList.add(R.drawable._12numberrev_6ft);
            imageList.add(R.drawable._13numberrev_6ft);
            imageList.add(R.drawable._14numberrev_6ft);
            imageList.add(R.drawable._15numberrev_6ft);
            imageList.add(R.drawable._16numberrev_6ft);
            imageList.add(R.drawable._17numberrev_6ft);
            imageList.add(R.drawable._18numberrev_6ft);
            tv7.setText("Numerics " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitycharthindi6ft60);
            imageList.add(R.drawable._2equitycharthindi6ft36);
            imageList.add(R.drawable._3equitycharthindi6ft24);
            imageList.add(R.drawable._4equitycharthindi6ft18);
            imageList.add(R.drawable._5equitycharthindi6ft12);
            imageList.add(R.drawable._6equitycharthindi6ft9);
            imageList.add(R.drawable._7equitycharthindi6ft6);
            imageList.add(R.drawable._8equitycharthindi6ft5);
            tv8.setText("Regional " + distance+ " ("+imageList.size()+")");
        }
        if ( preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1echartrev_6ft);
            imageList.add(R.drawable._2echartrev_6ft);
            imageList.add(R.drawable._3echartrev_6ft);
            imageList.add(R.drawable._4echartrev_6ft);
            imageList.add(R.drawable._5echartrev_6ft);
            imageList.add(R.drawable._6echartrev_6ft);
            imageList.add(R.drawable._7echartrev_6ft);
            imageList.add(R.drawable._8echartrev_6ft);
            imageList.add(R.drawable._9echartrev_6ft);
            tv9.setText("Tumbline E " + distance+ " ("+imageList.size()+")");
        }
        if ( preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome6fttumblinee200);
            imageList.add(R.drawable._2_duochrome6fttumblinee120);
            imageList.add(R.drawable._3_duochrome6fttumblinee80);
            imageList.add(R.drawable._4_duochrome6fttumblinee60);
            imageList.add(R.drawable._5_duochrome6fttumblinee40);
            imageList.add(R.drawable._6_duochrome6fttumblinee30);
            imageList.add(R.drawable._7_duochrome6fttumblinee20);
            imageList.add(R.drawable._8_duochrome6fttumblinee17);
            tv10.setText("Duo Tumbline E " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.isihara_common_1);
            imageList.add(R.drawable.isihara_common_2);
            imageList.add(R.drawable.isihara_common_3);
            imageList.add(R.drawable.isihara_common_4);
            imageList.add(R.drawable.isihara_common_5);
            imageList.add(R.drawable.isihara_common_6);
            imageList.add(R.drawable.isihara_common_7);
            imageList.add(R.drawable.isihara_common_8);
            imageList.add(R.drawable.isihara_common_9);
            imageList.add(R.drawable.isihara_common_10);
            imageList.add(R.drawable.isihara_common_11);
            tv11.setText("Isihara " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")){
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartenglish_rev008ft);
            imageList.add(R.drawable._2equitychart_english_rev008ft60);
            imageList.add(R.drawable._3equitychart_english_rev008ft36);
            imageList.add(R.drawable._4equitychart_english_rev008ft24);
            imageList.add(R.drawable._5equitychart_english_rev008ft18);
            imageList.add(R.drawable._6equitychart_english_rev008ft12);
            imageList.add(R.drawable._7equitychart_english_rev008ft9);
            imageList.add(R.drawable._8equitychart_english_rev008ft6);
            imageList.add(R.drawable._9equitychart_english_rev008ft5);
            imageList.add(R.drawable._10equitychartenglish_rev008ft);
            imageList.add(R.drawable._11equitychart_english_rev008ft60);
            imageList.add(R.drawable._12equitychart_english_rev008ft36);
            imageList.add(R.drawable._13equitychart_english_rev008ft24);
            imageList.add(R.drawable._14equitychart_english_rev008ft18);
            imageList.add(R.drawable._15equitychart_english_rev008ft12);
            imageList.add(R.drawable._16equitychart_english_rev008ft9);
            imageList.add(R.drawable._17equitychart_english_rev008ft6);
            imageList.add(R.drawable._18equitychart_english_rev008ft5);

            tv1.setText("Alphabets " + distance+ " ("+imageList.size()+")");

        } if ( preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1landoltcsingle8_60);
            imageList.add(R.drawable._2landoltcsingle8_36);
            imageList.add(R.drawable._3landoltcsingle_8_24);
            imageList.add(R.drawable._4landoltcsingl8_18);
            imageList.add(R.drawable._5landoltsingle8_12);
            imageList.add(R.drawable._6landoltsingl8_9);
            imageList.add(R.drawable._7landoltsingl8_6);
            imageList.add(R.drawable._8landoltsingl8_5);
            tv2.setText("Landolt C " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschart8ft200);
            imageList.add(R.drawable._2dotschart8ft120);
            imageList.add(R.drawable._3dotschart8ft80);
            imageList.add(R.drawable._4dotschart8ft60);
            imageList.add(R.drawable._5dotschart8ft40);
            imageList.add(R.drawable._6dotschart8ft30);
            imageList.add(R.drawable._7dotschart8ft20);
            imageList.add(R.drawable._8dotschart8ft17);
            tv3.setText("Dots " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome8ft200);
            imageList.add(R.drawable._2duochrome8ft120);
            imageList.add(R.drawable._3duochrome8ft80);
            imageList.add(R.drawable._4duochrome8ft60);
            imageList.add(R.drawable._5duochrome8ft40);
            imageList.add(R.drawable._6duochrome8ft30);
            imageList.add(R.drawable._7duochrome8ft20);
            imageList.add(R.drawable._8duochrome8ft17);
            tv4.setText("Duochrome " + distance+ " ("+imageList.size()+")");
           /* imageList.add(R.drawable.equitychartlogmar_8ft);
            tv4.setText("Longmar " + distance+ " ("+imageList.size()+")");*/

        } if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome8ft200);
            imageList.add(R.drawable._2_duochrome8ft120);
            imageList.add(R.drawable._3_duochrome8ft80);
            imageList.add(R.drawable._4_duochrome8ft60);
            imageList.add(R.drawable._5_duochrome8ft40);
            imageList.add(R.drawable._6_duochrome8ft30);
            imageList.add(R.drawable._7_duochrome8ft20);
            imageList.add(R.drawable._8_duochrome8ft17);
            tv5.setText("Duo Numeric " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartlogmar_8ft);
            tv6.setText("Logmar " + distance+ " ("+imageList.size()+")");
           // tv6.setText("Longmar " + distance+ " ("+imageList.size()+")");

        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev00_8ft);
            imageList.add(R.drawable._2numberrev00_8ft);
            imageList.add(R.drawable._3numberrev00_8ft);
            imageList.add(R.drawable._4numberrev00_8ft);
            imageList.add(R.drawable._5numberrev00_8ft);
            imageList.add(R.drawable._6numberrev00_8ft);
            imageList.add(R.drawable._7numberrev00_8ft);
            imageList.add(R.drawable._8numberrev00_8ft);
            imageList.add(R.drawable._9numberrev00_8ft);
            imageList.add(R.drawable._10numberrev00_8ft);
            imageList.add(R.drawable._11numberrev00_8ft);
            imageList.add(R.drawable._12numberrev00_8ft);
            imageList.add(R.drawable._13numberrev00_8ft);
            imageList.add(R.drawable._14numberrev00_8ft);
            imageList.add(R.drawable._15numberrev00_8ft);
            imageList.add(R.drawable._16numberrev00_8ft);
            imageList.add(R.drawable._17numberrev00_8ft);
            imageList.add(R.drawable._18numberrev00_8ft);
            tv7.setText("Numerics " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitycharthindi8ft60);
            imageList.add(R.drawable._2euitycharthindi8ft36);
            imageList.add(R.drawable._3equitycharthindi8ft24);
            imageList.add(R.drawable._4equitycharthindi8ft16);
            imageList.add(R.drawable._5equitycharthindi8ft12);
            imageList.add(R.drawable._6equitycharthindi8ft9);
            imageList.add(R.drawable._7equitycharthindi8ft6);
            imageList.add(R.drawable._8equitycharthindi8ft5);
            tv8.setText("Regional " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1echartrev008ft);
            imageList.add(R.drawable._2echartrev008ftrev60);
            imageList.add(R.drawable._3echartrev008ftrev36);
            imageList.add(R.drawable._4echartrev008ftrev24);
            imageList.add(R.drawable._5echartrev008ftrev18);
            imageList.add(R.drawable._6echartrev008ftrev12);
            imageList.add(R.drawable._7echartrev008ftrev9);
            imageList.add(R.drawable._8echartrev008ftrev6);
            imageList.add(R.drawable._9echartrev008ftrev5);
            tv9.setText("Tumbline E " + distance+ " ("+imageList.size()+")");
        }
        if(preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome8fttumblinee200);
            imageList.add(R.drawable._2_duochrome8fttumblinee120);
            imageList.add(R.drawable._3_duochrome8fttumblinee80);
            imageList.add(R.drawable._4_duochrome8fttumblinee60);
            imageList.add(R.drawable._5_duochrome8fttumblinee40);
            imageList.add(R.drawable._6_duochrome8fttumblinee30);
            imageList.add(R.drawable._7_duochrome8fttumblinee20);
            imageList.add(R.drawable._8_duochrome8fttumblinee17);
            tv10.setText("Duo Tumbline E " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.isihara_common_1);
            imageList.add(R.drawable.isihara_common_2);
            imageList.add(R.drawable.isihara_common_3);
            imageList.add(R.drawable.isihara_common_4);
            imageList.add(R.drawable.isihara_common_5);
            imageList.add(R.drawable.isihara_common_6);
            imageList.add(R.drawable.isihara_common_7);
            imageList.add(R.drawable.isihara_common_8);
            imageList.add(R.drawable.isihara_common_9);
            imageList.add(R.drawable.isihara_common_10);
            imageList.add(R.drawable.isihara_common_11);
            tv11.setText("Isihara " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")){
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitychart_rev10ft);
            imageList.add(R.drawable._2equitychart_rev10ft);
            imageList.add(R.drawable._3equitychart_rev10ft);
            imageList.add(R.drawable._4equitychart_rev10ft);
            imageList.add(R.drawable._5equitychart_rev10ft);
            imageList.add(R.drawable._6equitychart_rev10ft);
            imageList.add(R.drawable._7equitychart_rev10ft);
            imageList.add(R.drawable._8equitychart_rev10ft);
            imageList.add(R.drawable._9equitychart_rev10ft);
            imageList.add(R.drawable._10equitychart_rev10ft);
            imageList.add(R.drawable._11equitychart_rev10ft);
            imageList.add(R.drawable._12equitychart_rev10ft);
            imageList.add(R.drawable._13equitychart_rev10ft);
            imageList.add(R.drawable._14equitychart_rev10ft);
            imageList.add(R.drawable._15equitychart_rev10ft);
            imageList.add(R.drawable._16equitychart_rev10ft);
            imageList.add(R.drawable._17equitychart_rev10ft);
            imageList.add(R.drawable._18equitychart_rev10ft);
            tv1.setText("Alphabets " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1landoltcsingle10ft);
            imageList.add(R.drawable._2landoltcsingle10ft);
            imageList.add(R.drawable._3landoltcsingle10ft);
            imageList.add(R.drawable._4landoltcsingle10ft);
            imageList.add(R.drawable._5landoltcsingle10ft);
            imageList.add(R.drawable._6landoltcsingle10ft);
            imageList.add(R.drawable._7landoltcsingle10ft);
            imageList.add(R.drawable._8landoltcsingle10ft);
            tv2.setText("Landolt C " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschart10ft200);
            imageList.add(R.drawable._2dotschart10ft120);
            imageList.add(R.drawable._3dotschart10ft80);
            imageList.add(R.drawable._4dotschart10ft60);
            imageList.add(R.drawable._5dotschart10ft40);
            imageList.add(R.drawable._6dotschart10ft30);
            imageList.add(R.drawable._7dotschart10ft20);
            imageList.add(R.drawable._8dotschart10ft20);
            tv3.setText("Dots " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome10ft200);
            imageList.add(R.drawable._2duochrome10ft200);
            imageList.add(R.drawable._3duochrome10ft200);
            imageList.add(R.drawable._4duochrome10ft200);
            imageList.add(R.drawable._5duochrome10ft200);
            imageList.add(R.drawable._6duochrome10ft200);
            imageList.add(R.drawable._7duochrome10ft200);
            imageList.add(R.drawable._8duochrome10ft200);
            tv4.setText("Duochrome " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome10ft);
            imageList.add(R.drawable._2duochrome10ft);
            imageList.add(R.drawable._3duochrome10ft);
            imageList.add(R.drawable._4duochrome10ft);
            imageList.add(R.drawable._5duochrome10ft);
            imageList.add(R.drawable._6duochrome10ft);
            imageList.add(R.drawable._7duochrome10ft);
            imageList.add(R.drawable._8duochrome10ft);
            tv5.setText("Duo Numeric " + distance+ " ("+imageList.size()+")");


        } if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitychartlogmar10ft);
            tv6.setText("Logmar " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev10ft);
            imageList.add(R.drawable._2numberrev10ft);
            imageList.add(R.drawable._3numberrev10ft);
            imageList.add(R.drawable._4numberrev10ft);
            imageList.add(R.drawable._5numberrev10ft);
            imageList.add(R.drawable._6numberrev10ft);
            imageList.add(R.drawable._7numberrev10ft);
            imageList.add(R.drawable._8numberrev10ft);
            imageList.add(R.drawable._9numberrev10ft);
            imageList.add(R.drawable._10numberrev10ft);
            imageList.add(R.drawable._11numberrev10ft);
            imageList.add(R.drawable._12numberrev10ft);
            imageList.add(R.drawable._13numberrev10ft);
            imageList.add(R.drawable._14numberrev10ft);
            imageList.add(R.drawable._15numberrev10ft);
            imageList.add(R.drawable._16numberrev10ft);
            imageList.add(R.drawable._17numberrev10ft);
            imageList.add(R.drawable._18numberrev10ft);
            tv7.setText("Numerics " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitycharthindi10ft);
            imageList.add(R.drawable._2_1equitycharthindi10ft);
            imageList.add(R.drawable._3_1equitycharthindi10ft);
            imageList.add(R.drawable._4_1equitycharthindi10ft);
            imageList.add(R.drawable._5_1equitycharthindi10ft);
            imageList.add(R.drawable._6_1equitycharthindi10ft);
            imageList.add(R.drawable._7_1equitycharthindi10ft);
            imageList.add(R.drawable._8_1equitycharthindi10ft);
            tv8.setText("Regional " + distance+ " ("+imageList.size()+")");

        } if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1e_chart_rev10ft);
            imageList.add(R.drawable._2e_chart_rev10ft);
               imageList.add(R.drawable._3e_chart_rev10ft);
            imageList.add(R.drawable._4e_chart_rev10ft);
            imageList.add(R.drawable._5e_chart_rev10ft);
            imageList.add(R.drawable._6e_chart_rev10ft);
            imageList.add(R.drawable._7e_chart_rev10ft);
            imageList.add(R.drawable._8e_chart_rev10ft);
            imageList.add(R.drawable._9e_chart_rev10ft);
            tv9.setText("Tumbline E " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome10fttumblinee200);
            imageList.add(R.drawable._2_duochrome10fttumblinee120);
            imageList.add(R.drawable._3_duochrome10fttumblinee80);
            imageList.add(R.drawable._4_duochrome10fttumblinee60);
            imageList.add(R.drawable._5_duochrome10fttumblinee40);
            imageList.add(R.drawable._6_duochrome10fttumblinee30);
            imageList.add(R.drawable._7_duochrome10fttumblinee20);
            imageList.add(R.drawable._8_duochrome10fttumblinee20);
            tv10.setText("Duo Tumbline E " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.isihara_common_1);
            imageList.add(R.drawable.isihara_common_2);
            imageList.add(R.drawable.isihara_common_3);
            imageList.add(R.drawable.isihara_common_4);
            imageList.add(R.drawable.isihara_common_5);
            imageList.add(R.drawable.isihara_common_6);
            imageList.add(R.drawable.isihara_common_7);
            imageList.add(R.drawable.isihara_common_8);
            imageList.add(R.drawable.isihara_common_9);
            imageList.add(R.drawable.isihara_common_10);
            imageList.add(R.drawable.isihara_common_11);
            tv11.setText("Isihara " + distance+ " ("+imageList.size()+")");
        }
    }

    private void setData() {
        if (preferenceManager.getDistances().equals("5")) {
            initialScreen();
            } else if (preferenceManager.getDistances().equals("6")) {
            Common.folderName = new ArrayList<>();
            Common.folderImage = new ArrayList<>();

            Common.folderImage.add(R.drawable._1equity_chartenglishrev_6ft);
            Common.folderImage.add(R.drawable._1dotschart_6ft200);
            Common.folderImage.add(R.drawable._1duochrome_6ft200);
            Common.folderImage.add(R.drawable._1duochrome_numeric_6_ft);
            Common.folderImage.add(R.drawable._1landoltcsingle_6ft);
            Common.folderImage.add(R.drawable.equitychartlogma_6ft);
            Common.folderImage.add(R.drawable._1numberrev_6ft);
            Common.folderImage.add(R.drawable._1echartrev_6ft);



            Common.folderName.add("Alphabets " + distance);
            Common.folderName.add("Dots Chart " + distance);
            Common.folderName.add("Duochrome Chart " + distance);
            Common.folderName.add("Duochrome Numeric " + distance);
            Common.folderName.add("Landold C " + distance);
            Common.folderName.add("lOGMAR " + distance);
            Common.folderName.add("Numeric " + distance);
            Common.folderName.add("Tumbling E " + distance);
            Common.folderName.add("Duo Tumbling E " + distance);
            tv1.setText("Alphabets " + distance);
            tv2.setText("Dots Chart " + distance);
            tv3.setText("Duochrome Chart " + distance);
            tv4.setText("Duochrome Numeric " + distance);
            tv5.setText("Landold C " + distance);
            tv6.setText("lOGMAR " + distance);
            tv7.setText("Numeric " + distance);
            tv8.setText("Tumbling E " + distance);
            tv9.setText("Duo Tumbling E " + distance);

        } else if (preferenceManager.getDistances().equals("8")) {
           // layout3.setVisibility(View.GONE);
            Common.folderImage.add(R.drawable.equitychartenglish_rev008ft);
            Common.folderImage.add(R.drawable._1duochrome8ft200);
            Common.folderImage.add(R.drawable._1landoltcsingle8_60);
            Common.folderImage.add(R.drawable.equitychartlogmar_8ft);
            Common.folderImage.add(R.drawable._1numberrev00_8ft);
            Common.folderImage.add(R.drawable._1echartrev00_8ft);
            Common.folderImage.add(R.drawable._1_duochrome8ft200);
            Common.folderImage.add(R.drawable._1equitycharthindi8ft60);
            Common.folderName = new ArrayList<>();

            Common.folderName.add("Alphabets "+distance);
            Common.folderName.add("Duochrome "+distance);
            Common.folderName.add("Landolt C "+distance);
            Common.folderName.add("Logmar "+distance);
            Common.folderName.add("Numeric "+distance);
            Common.folderName.add("Tumbling E "+distance);
            Common.folderName.add("Duochrome Numeric "+distance);
            Common.folderName.add("Regional "+distance);
            tv1.setText("Alphabets "+distance);
            tv2.setText("Duochrome "+distance);
            tv3.setText("Landolt C "+distance);
            tv4.setText("Logmar "+distance);
            tv5.setText("Numerics "+distance);
            tv6.setText("Tumbling E "+distance);
            tv7.setText("Duochrome Numeric "+distance);
            tv8.setText("Regional "+distance);
            tv9.setText("Dots Chart "+distance);
            tv10.setText("Duo Tumbling E "+distance);
        }else if (preferenceManager.getDistances().equals("10")) {
            Common.folderImage.add(R.drawable._1equitychart_rev10ft);
            Common.folderImage.add(R.drawable._1equitychart_rev10ft);
            Common.folderImage.add(R.drawable._1duochrome10ft200);
            Common.folderImage.add(R.drawable._1duochrome10ft);
            Common.folderImage.add(R.drawable._1landoltcsingle10ft);
            Common.folderImage.add(R.drawable._1equitychartlogmar10ft);
            Common.folderImage.add(R.drawable._1numberrev10ft);
            Common.folderImage.add(R.drawable._1equitycharthindi10ft);
            Common.folderImage.add(R.drawable._1e_chart_rev10ft);

            Common.folderName.add("Alphabets "+distance);
            Common.folderName.add("Dots Chart "+distance);
            Common.folderName.add("Duochrome "+distance);
            Common.folderName.add("Duochrome Numeric "+distance);
            Common.folderName.add("Landolt C "+distance);
            Common.folderName.add("Logmar "+distance);
            Common.folderName.add("Numerics "+distance);
            Common.folderName.add("Regional "+distance);
            Common.folderName.add("Tumbling E "+distance);
            Common.folderName.add("Duo Tumbling E "+distance);
            tv1.setText("Alphabets "+distance);
            tv2.setText("Dots Chart "+distance);
            tv3.setText("Duochrome "+distance);
            tv4.setText("Duochrome Numeric "+distance);
            tv5.setText("Landolt C "+distance);
            tv6.setText("Logmar "+distance);
            tv7.setText("Numerics "+distance);
            tv8.setText("Regional "+distance);
            tv9.setText("Tumbling E "+distance);
            tv10.setText("Duo Tumbling E "+distance);
            tv11.setText("Isihara "+distance);
        } else {
            initialScreen();
        }
    }

    private void initialScreen() {
        Common.folderImage = new ArrayList<>();
        Common.folderName = new ArrayList<>();

        //Common.folderImage = new ArrayList<>();
        Common.folderImage.add(R.drawable._1equitychart_english_rev005ft);
        Common.folderImage.add(R.drawable._1landoltc_5ftsingle660);
        Common.folderImage.add(R.drawable._1dotschar5ft200);
        Common.folderImage.add(R.drawable._2_duochrome5ft120);
        Common.folderImage.add(R.drawable._2duochrome5ft120);
        Common.folderImage.add(R.drawable.equitychartlogmar5ft);
        Common.folderImage.add(R.drawable._1numberrev015ft);
        Common.folderImage.add(R.drawable._1echartrev005ftrev01);
        Common.folderImage.add(R.drawable._1equitycharthindi5ft60);
        Common.folderImage.add(R.drawable._1_duochrome5fttumbe200);

        Common.folderName.add("Alphabet Chart " + distance);
        Common.folderName.add("C Chart " + distance);
        Common.folderName.add("Dots " + distance);
        Common.folderName.add("DuoChrome " + distance);
        Common.folderName.add("DuoChrome Numeric " + distance);
        Common.folderName.add("Logmar " + distance);
        Common.folderName.add("Numeric Chart " + distance);
        Common.folderName.add("Regional " + distance);
        Common.folderName.add("Tumbling E Chart " + distance);
        tv1.setText("Alphabet Chart " + distance);
        tv2.setText("C Chart " + distance);
        tv3.setText("Dots " + distance);
        tv4.setText("DuoChrome " + distance);
        tv5.setText("DuoChrome Numeric " + distance);
        tv6.setText("Logmar " + distance);
        tv7.setText("Numeric Chart " + distance);
        tv8.setText("Regional " + distance);
        tv9.setText("Tumbling E Chart " + distance);
        tv10.setText("Duo Tumbling E " + distance);
        tv11.setText("Isihara " + distance);
    }
    private void setIcons() {
        if (preferenceManager.getDistances().equals("5")) {
            Glide.with(this).load(R.drawable._1equitychart_english_rev005ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img1);
            Glide.with(this).load(R.drawable._1landoltc_5ftsingle660)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img2);
            Glide.with(this).load(R.drawable._1dotschar5ft200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img3);
            Glide.with(this).load(R.drawable._2_duochrome5ft120)
                    .transform(new CenterCrop(),new RoundedCorners(30)).into(img4);
            Glide.with(this).load(R.drawable._2duochrome5ft120)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img5);
            Glide.with(this).load(R.drawable.equitychartlogmar5ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img6);
            Glide.with(this).load(R.drawable._1numberrev015ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img7);
            Glide.with(this).load(R.drawable._1equitycharthindi5ft60)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img8);
            Glide.with(this).load(R.drawable._1echartrev005ftrev01)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img9);
            Glide.with(this).load(R.drawable._1_duochrome5fttumbe200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img10);
            Glide.with(this).load(R.drawable.isihara_1_5)
                    .transform(new CenterCrop(),new RoundedCorners(30)).into(img11);


        } else if (preferenceManager.getDistances().equals("6")) {
            Glide.with(this).load(R.drawable._1equity_chartenglishrev_6ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img1);
            Glide.with(this).load(R.drawable._7landoltcsingle_6ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img2);
            Glide.with(this).load(R.drawable._1dotschart_6ft200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img3);
            Glide.with(this).load(R.drawable._1duochrome_6ft200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img4);
            Glide.with(this).load(R.drawable._2duochrome5ft120)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img5);
           /* Glide.with(this).load(R.drawable._1landoltcsingle_6ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img5);*/

            Glide.with(this).load(R.drawable.equitychartlogmar5ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img6);
            Glide.with(this).load(R.drawable._1numberrev_6ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img7);
          /*  Glide.with(this).load(R.drawable._1echartrev_6ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img8);
            Glide.with(this).load(R.drawable._1equitycharthindi6ft60)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img9);*/
            Glide.with(this).load(R.drawable._1equitycharthindi6ft60)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img8);
            Glide.with(this).load(R.drawable._1echartrev_6ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img9);
            Glide.with(this).load(R.drawable._1_duochrome6fttumblinee200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img10);
            Glide.with(this).load(R.drawable.isihara_common_1)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img11);

            // layout_setting.setVisibility(View.GONE);
            /*tv9.setText("Settings");
            //Ravi changes
           // tv9.setText("Settings");*/

        } else if (preferenceManager.getDistances().equals("8")) {
            Glide.with(this).load(R.drawable._1equity_chartenglishrev_6ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img1);
            Glide.with(this).load(R.drawable._1landoltcsingle8_60)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img2);
            Glide.with(this).load(R.drawable._1dotschart8ft200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img3);
            Glide.with(this).load(R.drawable._1duochrome8ft200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img4);
            Glide.with(this).load(R.drawable._1_duochrome8ft200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img5);
            Glide.with(this).load(R.drawable.equitychartlogmar5ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img6);
            Glide.with(this).load(R.drawable._2numberrev00_8ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img7);
            Glide.with(this).load(R.drawable._1equitycharthindi8ft60)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img8);
            Glide.with(this).load(R.drawable._1echartrev005ftrev01)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img9);

            Glide.with(this).load(R.drawable._1_duochrome8fttumblinee200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img10);

            Glide.with(this).load(R.drawable.isihara_common_1)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img11);

        } else if (preferenceManager.getDistances().equals("10")) {
            Glide.with(this).load(R.drawable._2equitychart_rev10ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img1);

            Glide.with(this).load(R.drawable._1landoltcsingle10ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img2);

            Glide.with(this).load(R.drawable._1dotschart10ft200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img3);
            Glide.with(this).load(R.drawable._1duochrome10ft200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img4);
            Glide.with(this).load(R.drawable._1duochrome10ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img5);
            Glide.with(this).load(R.drawable.equitychartlogmar5ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img6);
            Glide.with(this).load(R.drawable._2numberrev10ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img7);
            Glide.with(this).load(R.drawable._1equitycharthindi10ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img8);
            Glide.with(this).load(R.drawable._2e_chart_rev10ft)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img9);
            Glide.with(this).load(R.drawable._1_duochrome10fttumblinee200)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img10);
            Glide.with(this).load(R.drawable.isihara_common_1)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img11);
        }
    }
    private void initializeClicks() {
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "0"));
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "1"));
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "2"));
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "3"));
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "4"));
            }
        });
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "5"));
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "6"));
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, RegionalActivity.class).putExtra("screen", "7"));
                //startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "7"));
            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "8"));
            }
        });
        img10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "9"));
            }
        });
        img11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "10"));
            }
        });
        settingsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HomeScreen.class));
                preferenceManager.setDistances(0 + "");
            }
        });
        imgSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HomeScreen.class));
                preferenceManager.setDistances(0 + "");
            }
        });
        /*if(preferenceManager.getDistances().equals("6")){
            img9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, HomeScreen.class));
                    preferenceManager.setDistances(0 + "");
                }
            });
        }*/
    }
    private void initializeViews() {
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);
        img5 = findViewById(R.id.img5);
        img6 = findViewById(R.id.img6);
        img7 = findViewById(R.id.img7);
        img8 = findViewById(R.id.img8);
        img9 = findViewById(R.id.img9);
        img10 = findViewById(R.id.img10);
        img11 = findViewById(R.id.img11);

        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv4 = findViewById(R.id.tv4);
        tv5 = findViewById(R.id.tv5);
        tv6 = findViewById(R.id.tv6);
        tv7 = findViewById(R.id.tv7);
        tv8 = findViewById(R.id.tv8);
        tv9 = findViewById(R.id.tv9);
        tv10 = findViewById(R.id.tv10);
        tv11 = findViewById(R.id.tv11);

        settingsIV=findViewById(R.id.settingsIV);
        imgSetting = findViewById(R.id.imgSetting);
        layout3 = findViewById(R.id.layout3);
        layout_setting = findViewById(R.id.layout_setting);
       // Log.e("listSize", String.valueOf(imageList.size()));
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if(count==12){
                    startActivity(new Intent(MainActivity.this, HomeScreen.class)); //settings
                    preferenceManager.setDistances(0 + "");
                }
                else if(count==8){
                    startActivity(new Intent(MainActivity.this, RegionalActivity.class));
                }
                else{
                    startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", String.valueOf(count-1)));
                }
                //Toast.makeText(getApplicationContext(),"DPAD",Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_0:
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "0"));
                return true;
            case KeyEvent.KEYCODE_1:
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "1"));
                return true;
            case KeyEvent.KEYCODE_2:
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "2"));
                return true;
            case KeyEvent.KEYCODE_3:
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "3"));
                return true;
            case KeyEvent.KEYCODE_4:
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "4"));
                return true;
            case KeyEvent.KEYCODE_5:
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "5"));
                return true;
            case KeyEvent.KEYCODE_6:
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "6"));
                return true;
            case KeyEvent.KEYCODE_7:
                startActivity(new Intent(MainActivity.this, RegionalActivity.class).putExtra("screen", "7"));
                return true;
            case KeyEvent.KEYCODE_8:
                startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "8"));
                return true;
            case KeyEvent.KEYCODE_9:
                if(preferenceManager.getDistances().equals("8")){
                    Toast.makeText(getApplicationContext(),"Please choose number less than 9",Toast.LENGTH_SHORT).show();
                    //startActivity(new Intent(MainActivity.this, HomeScreen.class)); //settings
                    //preferenceManager.setDistances(0 + "");
                }
                else{
                    startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "9"));
                }
                /*if(preferenceManager.getDistances().equals("6")){
                    startActivity(new Intent(MainActivity.this, HomeScreen.class)); //settings
                    preferenceManager.setDistances(0 + "");
                }else{
                    startActivity(new Intent(MainActivity.this, FullImageActivity.class).putExtra("screen", "9"));
                }*/
                return true;
            //Ravi changes
           /* case KeyEvent.KEYCODE_0:
                startActivity(new Intent(MainActivity.this, HomeScreen.class)); //settings
                preferenceManager.setDistances(0 + "");
                return true;*/
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if(preferenceManager.getDistances().equals("5")){
                    if(count<12){
                        count++;
                        Log.e("itemcount", String.valueOf(count));
                        if(count==2){
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==3){
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==4){
                            fourLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==5){
                            fiveLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==6){
                            sixLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==7){
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==8){
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==9){
                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==10){
                            tenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            Log.e("countlayout", String.valueOf(count));
                        }
                        else if(count==11){
                            elevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            Log.e("countlayout", String.valueOf(count));
                        }
                        else if(count==12){
                            settingsLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            Log.e("countlayout", String.valueOf(count));
                        }
                    }

                }
                else if(preferenceManager.getDistances().equals("6")){
                    if(count<12){
                        count++;
                        Log.e("itemcount", String.valueOf(count));
                        if(count==2){
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==3){
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==4){
                            fourLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==5){
                            fiveLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==6){
                            sixLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==7){
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==8){
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==9){
                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==10){
                            tenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==11){
                            elevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==12){
                            settingsLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                    }
                }
                else if(preferenceManager.getDistances().equals("8")){
                    if(count<12){
                        count++;
                        Log.e("itemcount", String.valueOf(count));
                        if(count==2){
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==3){
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==4){
                            fourLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==5){
                            fiveLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==6){
                            sixLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==7){
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==8){
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==9){
                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==10){
                            tenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==11){
                            elevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==12){
                            settingsLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
//                       /* else if(count==9){
//                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
//                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
//                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
//                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
//                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
//                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
//                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
//                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
//                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
//                        }*/
                    }
                }
                else if(preferenceManager.getDistances().equals("10")){
                    if(count<12){
                        count++;
                        Log.e("itemcount", String.valueOf(count));
                        if(count==2){
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==3){
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==4){
                            fourLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==5){
                            fiveLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==6){
                            sixLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==7){
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==8){
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==9){
                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==10){
                            tenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==11){
                            elevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==12){
                            settingsLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                    }
                }
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if(preferenceManager.getDistances().equals("5")){
                    if(count>1){
                        count--;
                        Log.e("itemcount", String.valueOf(count));
                        if(count==1){
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        if(count==2){
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==3){
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==4){
                            fourLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==5){
                            fiveLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==6){
                            sixLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==7){
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==8){
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==9){
                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==10){
                            tenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==11){
                            elevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==12){
                            settingsLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                    }
                }
                else if(preferenceManager.getDistances().equals("6")){
                    if(count>1){
                        count--;
                        Log.e("itemcount", String.valueOf(count));
                        if(count==1){
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        if(count==2){
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==3){
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==4){
                            fourLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==5){
                            fiveLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==6){
                            sixLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==7){
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==8){
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==9){
                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));

                        }
                        else if(count==10){
                            tenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==11){
                            elevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==12){
                            settingsLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                    }
                }
                else if(preferenceManager.getDistances().equals("8")){
                    if(count>1){
                        count--;
                        Log.e("itemcount", String.valueOf(count));
                        if(count==1){
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        if(count==2){
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==3){
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==4){
                            fourLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==5){
                            fiveLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==6){
                            sixLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==7){
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==8){
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==9){
                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==10){
                            tenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==11){
                            elevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==12){
                            settingsLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                       /* else if(count==9){
                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }*/
                    }
                }
                else if(preferenceManager.getDistances().equals("10")){
                    if(count>1){
                        count--;
                        Log.e("itemcount", String.valueOf(count));
                        if(count==1){
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        if(count==2){
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==3){
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==4){
                            fourLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==5){
                            fiveLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==6){
                            sixLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==7){
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==8){
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==9){
                            nineLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==10){
                            tenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==11){
                            elevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            settingsLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(count==12){
                            settingsLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            tenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            nineLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fiveLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            elevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                    }
                }
                return true;
            case KeyEvent.KEYCODE_BACK:
                /*MainActivity.this.finish();
                finish();*/
               // startActivity(new Intent(MainActivity.this, HomeScreen.class));
              //  preferenceManager.setDistances(0 + "");
              //  finish();
               /* if(distance.equalsIgnoreCase("5ft")){
                    preferenceManager.setDistances(5 + "");
                }
                else if(distance.equalsIgnoreCase("6ft")){
                    preferenceManager.setDistances(6 + "");
                }
                else if(distance.equalsIgnoreCase("8ft")){
                    preferenceManager.setDistances(8 + "");
                }
                else if(distance.equalsIgnoreCase("10ft")){
                    preferenceManager.setDistances(10 + "");
                }*/
               // Intent in=new Intent(MainActivity.this,HomeScreen.class);
               // preferenceManager.setDistances(0 + "");
               // in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //startActivity(in);
               // finishAffinity();
                break;
        }
        return true;
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        if(distance.equalsIgnoreCase("5ft")){
            preferenceManager.setDistances(5 + "");
        }
        else if(distance.equalsIgnoreCase("6ft")){
            preferenceManager.setDistances(6 + "");
        }
        else if(distance.equalsIgnoreCase("8ft")){
            preferenceManager.setDistances(8 + "");
        }
        else if(distance.equalsIgnoreCase("10ft")){
            preferenceManager.setDistances(10 + "");
        }
        finishAffinity();
    }*/
}
