package com.vikash.imagedemoapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vikash.imagedemoapp.R;

import java.util.ArrayList;

public class AdapterDistance extends RecyclerView.Adapter<AdapterDistance.ViewHolder> {

    ArrayList<String> distance;
    private onClickListener onClickListener;

    public AdapterDistance(ArrayList<String> distance, onClickListener onClickListener) {
        this.distance = distance;
        this. onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_distance, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.button.setText(distance.get(position).replace("_","")+"");
    }

    @Override
    public int getItemCount() {
        return distance.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button button;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
    public interface onClickListener{
        void onClick(int position);
    }
}
