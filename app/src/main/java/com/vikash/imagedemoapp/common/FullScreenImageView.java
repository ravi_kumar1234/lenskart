package com.vikash.imagedemoapp.common;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.vikash.imagedemoapp.R;

public class FullScreenImageView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image_view);
        String url =getIntent().getStringExtra("url");
        ImageView fullImage=findViewById(R.id.fullImage);
        Glide.with(this).load(url).centerCrop().fitCenter().into(fullImage);
        ImageView closeBtn=findViewById(R.id.closeBtn);

            Glide.with(this).load(R.drawable.close_btn).into(closeBtn);

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
