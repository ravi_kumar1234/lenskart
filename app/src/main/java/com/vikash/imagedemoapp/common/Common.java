package com.vikash.imagedemoapp.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.vikash.imagedemoapp.model.Imagemodel;
import com.vikash.imagedemoapp.screen1.HomeScreen;

import java.util.ArrayList;


public class Common {
    public static AlertDialog testDialog;
    public static ProgressDialog progressDialog;
    public static Boolean editClicked = false;
    public static String isNOTIFICATION = "notification_clicked";
    public static String profileId = "";
    public static Imagemodel imageData = new Imagemodel();
    public static String distance_size = "0";
    public static ArrayList<String> folderName = new ArrayList<>();
    public static ArrayList<Integer> folderImage = new ArrayList<>();

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public SnapHelper snaphelper(){
        LinearSnapHelper snapHelper;
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                View centerView = findSnapView(layoutManager);
                if (centerView == null)
                    return RecyclerView.NO_POSITION;

                int position = layoutManager.getPosition(centerView);
                int targetPosition = -1;
                if (layoutManager.canScrollHorizontally()) {
                    if (velocityX < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                if (layoutManager.canScrollVertically()) {
                    if (velocityY < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                final int firstItem = 0;
                final int lastItem = layoutManager.getItemCount() - 1;
                targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));
                return targetPosition;
            }
        };
        return snapHelper;
    }
 /*   public static boolean isConnectedToInternet(Context context) {
        boolean connected = false;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) (context.getApplicationContext())
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager
                    .getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable()
                    && networkInfo.isConnected();
            if (connected)
                return connected;
            //else showNoInternetDialog(context);

        } catch (Exception e) {
            System.out
                    .println("CheckConnectivity Exception: " + e.getMessage());
            Log.v("connectivity", e.toString());
        }
        return connected;
    }
*/
    /*public static boolean checkConnection(Activity context) {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            return true;

        } else {
            androidx.appcompat.app.AlertDialog.Builder al = new androidx.appcompat.app.AlertDialog.Builder(context);
            al.setMessage("No internet connection");
            al.setTitle("NO INTERNET");
            al.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    context.finish();
                    context.startActivity(context.getIntent());
                }
            });
            al.setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    context.finish();
                    System.exit(0);
                }
            });
            al.setCancelable(false);
            al.show();
            return false;
            //setContentView(R.layout.internet_screen);
        }
    }*/


    public static void alertDialog(Context context, String title, String message) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        alertbox.setTitle(title)
                .setCancelable(false).setMessage(message).
                setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        alertbox.show();
    }

    public static void progressDialog(Context context, String message) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void progressDialogClose() {
        progressDialog.dismiss();

    }

    public static void fullScreencall(Window window, Context context) {
        window.setFlags(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = window.getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = window.getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    public static void fullScreenImageDialog(Context context, Drawable imageUrl) {
        Intent intent=new Intent(context, FullScreenImageView.class);
//        intent.putExtra("url",imageUrl);
        context.startActivity(intent);
        /* AlertDialog.Builder alertDialog;
        ImageView closeImg;
        alertDialog = new AlertDialog.Builder(context);
        final View views = LayoutInflater.from(context).inflate(R.layout.image_change_profile, null);
        alertDialog.setView(views);
        ImageView imageView = views.findViewById(R.id.imageView);

        closeImg = views.findViewById(R.id.closeImg);
        if (isCustomer == 0) {
            Glide.with(context).load(R.drawable.close_icon_green).into(closeImg);
        } else {
            Glide.with(context).load(R.drawable.close_btn).into(closeImg);
        }
        Glide.with(context).load(imageUrl).centerCrop().fitCenter().into(imageView);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testDialog.dismiss();
            }
        });
        testDialog = alertDialog.create();

        testDialog.show();
    */}
/*
    public static void fullScreenImageDialogForUri(Context context, Uri imageUrl, int isCustomer) {
        AlertDialog.Builder alertDialog;
        ImageView closeImg;
        alertDialog = new AlertDialog.Builder(context);
        final View views = LayoutInflater.from(context).inflate(R.layout.image_change_profile, null);
        alertDialog.setView(views);
        ImageView imageView = views.findViewById(R.id.imageView);

        closeImg = views.findViewById(R.id.closeImg);
        if (isCustomer == 0) {
            Glide.with(context).load(R.drawable.close_icon_green).into(closeImg);
        } else {
            Glide.with(context).load(R.drawable.close_btn).into(closeImg);
        }
        Glide.with(context).load(imageUrl).centerCrop().fitCenter().into(imageView);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testDialog.dismiss();
            }
        });
        testDialog = alertDialog.create();

        testDialog.show();
    }
*/
    public static void printToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
