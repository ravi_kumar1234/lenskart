package com.vikash.imagedemoapp.screen1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.internal.LinkedTreeMap;
import com.vikash.imagedemoapp.Gallery;
import com.vikash.imagedemoapp.MainActivity;
import com.vikash.imagedemoapp.common.PreferenceManager;
import com.vikash.imagedemoapp.R;
import com.vikash.imagedemoapp.common.Common;
import com.vikash.imagedemoapp.remote.network.RestClient;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeScreen extends AppCompatActivity {
    private Button buttonoffline, buttonrefresh, buttonToken, buttonDistance, buttonNext;
    private String[] permissionsArray = {Manifest.permission.READ_PHONE_STATE};
    private static final int REQUEST_CODE = 1;
    private PreferenceManager preferenceManager;
    private ArrayList<String> distance;
    private ArrayList<String> folder_array;
    private ArrayList<Bitmap> image_Bitmap = new ArrayList<>();
    private HashMap<String, ArrayList<String>> folder = new HashMap<>();
    private HashMap<String, HashMap<String, ArrayList<Bitmap>>> hashMapofImages = new HashMap<>();
    private HashMap<String, ArrayList<Bitmap>> hashMapofOnlyImages = new HashMap<>();
    private static String BASE_URL_IMAGE = "http://way2sim.com/digitalp/img/";
    private Bitmap bitmap = null;
    private EditText et_token, et_distance;
    private RelativeLayout layout_token;
    private String token = "", deviceId = "";
    private TextView tv1, tv2, tv3, tv4, tv5, tv_removeSelected;
    private int distance_selected = 0;
    private boolean selected = false;
    int selectcount=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Common.fullScreencall(getWindow(), this);
        Common.hideKeyboard(this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_homescreen);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        Intent launchintent=getPackageManager().getLaunchIntentForPackage("com.vikash.imagedemo");
        if(launchintent!=null){
            startActivity(launchintent);
        }
        preferenceManager = new PreferenceManager(this);
        distance = new ArrayList<>();
        folder_array = new ArrayList<>();
        folder = new HashMap<>();
        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        initializeViews();
        initializeClicks();
        if (!preferenceManager.getToken().equals("")) {
            et_token.setText(preferenceManager.getToken());
            layout_token.setVisibility(View.GONE);
        } else {
            layout_token.setVisibility(View.VISIBLE);
        }

        if (preferenceManager.getDistances().equals("")) {
            distance_selected = 5;
            tv1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else if (preferenceManager.getDistances().equals("0")) {
            distance_selected = 5;
            tv1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else {
            selectDistance(Integer.parseInt(preferenceManager.getDistances()), tv1);
            startActivity(new Intent(HomeScreen.this, MainActivity.class).putExtra("distance", preferenceManager.getDistances()));
            finish();
        }
    }
    private void initializeClicks() {
        et_token.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() == 8){
                    token = et_token.getText().toString();
                    Common.hideKeyboard(HomeScreen.this);
                    registerDevice(token);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        buttonToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(et_token.getText().toString())) {
                    token = et_token.getText().toString();
                    Common.hideKeyboard(HomeScreen.this);
                    registerDevice(token);
                } else {
                    Common.printToast(HomeScreen.this, "Enter your token");
                }
            }
        });
        buttonDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(et_distance.getText().toString())) {
                    preferenceManager.setDistances(et_distance.getText().toString());
                }
            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (!preferenceManager.getToken().equals("")) {
                    if (preferenceManager.getDistances().equals("")) {
                        preferenceManager.setDistances(5+"");
                        startActivity(new Intent(HomeScreen.this, MainActivity.class));
                        finish();

                    } else if (preferenceManager.getDistances().equals("0")) {
                        preferenceManager.setDistances(5+"");
                        startActivity(new Intent(HomeScreen.this, MainActivity.class));
                        finish();
                    }
                    else {
                       // Toast.makeText(getApplicationContext(),"Main class",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(HomeScreen.this, MainActivity.class)
                                .putExtra("distance", preferenceManager.getDistances()));
                         finish();
                    }
/*
                    if(!preferenceManager.getDistances().equals("0")){
                        startActivity(new Intent(HomeScreen.this, Gallery.class).putExtra("screen",preferenceManager.getDistances()));

                    }else{
                        startActivity(new Intent(HomeScreen.this, MainActivity.class));

                    }
*/
                } else {
                    Common.printToast(HomeScreen.this, "Verify your token");
                }
            }
        });
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDistance(5, tv1);
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDistance(6, tv2);
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDistance(8, tv3);
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDistance(10, tv4);
            }
        });
/*        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDistance(4, tv4);
            }
        });
        tv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDistance(5, tv5);
            }
        });*/
        tv_removeSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText()
                selectDistance(0, tv5);
              //  tv_removeSelected.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
            }
        });
    }
    private void registerDevice(final String token) {
        RestClient.getApiInterface().registerDevice(token, deviceId).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.d("TAG", "onResponse: " + response.body());
                //((LinkedTreeMap) response.body).get("response")
                if (((LinkedTreeMap) response.body()).get("response").equals("invalid")) {
                    layout_token.setVisibility(View.VISIBLE);
                    new AlertDialog.Builder(HomeScreen.this)
                            .setTitle(R.string.error)
                            .setMessage(R.string.alert_msg)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).show();

                } else {
                    layout_token.setVisibility(View.GONE);
                    Common.printToast(HomeScreen.this, ((LinkedTreeMap) response.body()).get("response").toString());
                    preferenceManager.setToken(token);
                }
            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }
    private void initializeViews() {
        buttonToken = findViewById(R.id.buttonToken);
        et_token = findViewById(R.id.et_token);
        layout_token = findViewById(R.id.layout_token);
        buttonDistance = findViewById(R.id.buttonDistance);
        et_distance = findViewById(R.id.et_distance);
        buttonNext = findViewById(R.id.buttonNext);
        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv4 = findViewById(R.id.tv4);
        tv5 = findViewById(R.id.tv5);
        tv_removeSelected = findViewById(R.id.tv_removeSelected);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                preferenceManager.setPermission(true);
                Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(HomeScreen.this, permissionsArray, REQUEST_CODE);
                Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
//                verifyAllPermissions();
            }
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {//Toast.makeText(getApplicationContext(),"key pressed is:" +keyCode ,Toast.LENGTH_SHORT).show();
        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
               // Toast.makeText(getApplicationContext(),"key pressed is: enter pressed" +keyCode ,Toast.LENGTH_SHORT).show();
                if (!preferenceManager.getToken().equals("")) {
                    if (!TextUtils.isEmpty(et_token.getText().toString())) {
                        token = et_token.getText().toString();
                        registerDevice(token);
                    } else {
                        Common.printToast(HomeScreen.this, "Enter your token");
                    }
                }
                break;
            case KeyEvent.KEYCODE_1:
                //Toast.makeText(getApplicationContext(),"key pressed is: 1 pressed" +keyCode ,Toast.LENGTH_SHORT).show();
                selectDistance(5, tv1);
                selectcount=1;
                break;
            case KeyEvent.KEYCODE_2:
                selectDistance(6, tv2);
                selectcount=2;
                break;
            case KeyEvent.KEYCODE_3:
                selectDistance(8, tv3);
                selectcount=3;
                break;
            case KeyEvent.KEYCODE_4:
                selectDistance(10, tv4);
                selectcount=4;
                break;
            case KeyEvent.KEYCODE_5:
                selectDistance(0, tv_removeSelected);
               // selectcount=5;
                break;
              case KeyEvent.KEYCODE_DPAD_RIGHT:
                  if(selectcount<5){
                      selectcount++;
                      if(selectcount==2){
                          selectDistance(6, tv2);
                          Log.e("selectCount", String.valueOf(selectcount));
                         /* tv4.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                          tv1.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv5.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv3.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv4.setBackgroundColor(getResources().getColor(R.color.gray_verylight));*/
                      }
                      else if(selectcount==3){
                          selectDistance(8, tv3);
                          Log.e("selectCount", String.valueOf(selectcount));
                        /*  tv3.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                          tv1.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv2.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv4.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv5.setBackgroundColor(getResources().getColor(R.color.gray_verylight));*/
                      }
                      else if(selectcount==4){
                          selectDistance(10, tv4);
                          Log.e("selectCount", String.valueOf(selectcount));
                         /* tv4.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                          tv1.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv2.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv3.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv5.setBackgroundColor(getResources().getColor(R.color.gray_verylight));*/
                      }
                     /* else if(selectcount==5){
                          selectDistance(0, tv_removeSelected);
                          Log.e("selectCount", String.valueOf(selectcount));
                        *//*  tv5.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                          tv1.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv2.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv3.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
                          tv4.setBackgroundColor(getResources().getColor(R.color.gray_verylight));*//*
                      }*/
                  }
               // startActivity(new Intent(HomeScreen.this, MainActivity.class));
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if(selectcount>1){
                    selectcount--;
                      if(selectcount==1){
                          selectDistance(5, tv1);
                          Log.e("selectCount", String.valueOf(selectcount));
                      }
                      else if(selectcount==2){
                          selectDistance(6, tv2);
                          Log.e("selectCount", String.valueOf(selectcount));
                      }
                      else if(selectcount==3){
                          selectDistance(8, tv3);
                          Log.e("selectCount", String.valueOf(selectcount));
                      }
                      else if(selectcount==4){
                          selectDistance(10, tv4);
                          Log.e("selectCount", String.valueOf(selectcount));
                      }
                }
               // startActivity(new Intent(HomeScreen.this, MainActivity.class));
                break;
            case KeyEvent.KEYCODE_0:
                startActivity(new Intent(HomeScreen.this, MainActivity.class));
                finish();
                break;
            case KeyEvent.KEYCODE_ESCAPE:
                finish();
                break;
        }
        return true;
    }
    private void selectDistance(int value, TextView tv) {
        preferenceManager.setDistances(value + "");
        if (value == 5) {
            distance_selected = value;
            tv1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else {
            tv1.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
        }
        if (value == 6) {
            distance_selected = value;
            tv2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else {
            tv2.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
        }
        if (value == 8) {
            distance_selected = value;
            tv3.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else {
            tv3.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
        }
        if (value == 10) {
            distance_selected = value;
            tv4.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else {
            tv4.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
        }
        /*if (value == 0) {
            distance_selected = value;
            tv_removeSelected.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else {
            tv_removeSelected.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
        }*/
        /*if (value == 4) {
            distance_selected = value;
            tv4.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else {
            tv4.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
        }
        if (value == 5) {
            distance_selected = value;
            tv5.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else {
            tv5.setBackgroundColor(getResources().getColor(R.color.gray_verylight));
        }*/
    }

}