package com.vikash.imagedemoapp.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.target.Target;
import com.vikash.imagedemoapp.R;
import com.vikash.imagedemoapp.common.Common;

import java.util.ArrayList;

/**
 * Created by Vikash Kumar on 7/10/2020
 */
public class AdapterGallery extends RecyclerView.Adapter<AdapterGallery.ViewHolder> {
    private Context context;
    private ArrayList<Integer> imageList;
    public static AlertDialog testDialog;
    private onClickListener onClickListener;
    private ArrayList<String> imageName;

    public AdapterGallery(ArrayList<Integer> imageList, ArrayList<String> imageName, onClickListener onClickListener) {
        this.imageList = imageList;
        this.imageName = imageName;
        imageList = new ArrayList<>();
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_imageiew, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try{
            Glide.with(context).load(imageList.get(position))
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(holder.imgView);

        }catch (Exception e){}
        try{
            holder.tv.setText(imageName.get(position));

        }catch (Exception e){}
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgView;
        TextView tv;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgView = itemView.findViewById(R.id.imgView);
            tv = itemView.findViewById(R.id.tv);
            imgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.onClick(getAdapterPosition());

//                    Common.fullScreenImageDialog(context, imageList.get(getAdapterPosition()));

                }
            });
        }
    }

    public interface onClickListener{
        void onClick(int position);
    }
}
