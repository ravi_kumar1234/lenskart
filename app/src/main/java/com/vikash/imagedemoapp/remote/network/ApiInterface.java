package com.vikash.imagedemoapp.remote.network;



import com.vikash.imagedemoapp.model.Imagemodel;

import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Developer: Click Labs
 * <p>
 * The API interface for your application
 */
public interface ApiInterface {

    String IMAGE_GET="categories/refreshapp/1234";
    /*
        @FormUrlEncoded
        @POST(SIGNUP)
        Call<LoginModel> signup(@Field("name") String name, @Field("email") String email, @Field("password") String password);
    */

    @GET("categories/registerdevice/{token}/{deviceId}")
    Call<Object> registerDevice(@Path("token") String token, @Path("deviceId")String deviceId);

}