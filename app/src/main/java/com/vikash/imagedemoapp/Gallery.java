package com.vikash.imagedemoapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.vikash.imagedemoapp.adapter.AdapterGallery;
import com.vikash.imagedemoapp.common.Common;
import com.vikash.imagedemoapp.common.PreferenceManager;

import java.util.ArrayList;


public class Gallery extends AppCompatActivity implements AdapterGallery.onClickListener {
    private AdapterGallery adapterGallery;
    private RecyclerView recyclerview;
    private ArrayList<Integer> imageList;
    private ArrayList<String> imageName;
    public static AlertDialog testDialog;
    private int pos = 0;
    private String screen = "0";
    private PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Common.fullScreencall(getWindow(), this);
        setContentView(R.layout.activity_gallery);

        preferenceManager = new PreferenceManager(this);
        recyclerview = findViewById(R.id.recyclerview);
        imageList = new ArrayList<>();
        imageName = new ArrayList<>();
        screen = getIntent().getStringExtra("screen");
        if (screen.equals("1") && preferenceManager.getDistances().equals("5")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitychart_english_rev005ft);
            imageList.add(R.drawable._2equitychart_english_rev005ft60);
            imageList.add(R.drawable._3equitychart_english_rev005ft36);
            imageList.add(R.drawable._4equitychart_english_rev005ft24);
            imageList.add(R.drawable._5equitychart_english_rev005ft18);
            imageList.add(R.drawable._6equitychart_english_rev005ft12);
            imageList.add(R.drawable._7equitychart_english_rev005ft9);
            imageList.add(R.drawable._8equitychart_english_rev005ft6);

            imageName.add("1  Equity Chart- ENGLISH Rev 00  5 FT");
            imageName.add("2  Equity Chart- ENGLISH Rev 00  5 FT 60");
            imageName.add("3  Equity Chart- ENGLISH Rev 00  5 FT 36");
            imageName.add("4  Equity Chart- ENGLISH Rev 00  5 FT 24");
            imageName.add("5  Equity Chart- ENGLISH Rev 00  5 FT 18");
            imageName.add("6  Equity Chart- ENGLISH Rev 00  5 FT 12");
            imageName.add("7   Equity Chart- ENGLISH Rev 00  5 FT 9");
            imageName.add("8  Equity Chart- ENGLISH Rev 00  5 FT 6");
        } else if (screen.equals("2") && preferenceManager.getDistances().equals("5")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
//            imageList.add(Common.folderImage.get(1));
            imageList.add(R.drawable._1landoltc_5ftsingle660);
            imageList.add(R.drawable._2landoltc_5ftsingle636);
            imageList.add(R.drawable._3landoltc_5ftsingle624);
            imageList.add(R.drawable._4landoltc_5ftsingle618);
            imageList.add(R.drawable._5landoltc_5ftsingle612);
            imageList.add(R.drawable._6landoltc_5ftsingle69);

            imageName.add("1  Landolt C 5 ft single 6-60");
            imageName.add("2  Landolt 5 ft C single 6-36");
            imageName.add("3  Landolt C 5 ft single 6-24");
            imageName.add("4  Landolt 5 ft C single 6-18");
            imageName.add("5  Landolt 5 ft C single 6-12");
            imageName.add("6  Landolt 5ft C single 6-9");

        } else if (screen.equals("3") && preferenceManager.getDistances().equals("5")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschar5ft200);
            imageList.add(R.drawable._2dotschart5ft120);
            imageList.add(R.drawable._3dotschart5ft80);
            imageList.add(R.drawable._4dotschart5f60);

            imageName.add("1  Dots chart 5 ft 200");
            imageName.add("2  Dots chart 6 ft 120");
            imageName.add("3  Dots chart 5 ft 80");
            imageName.add("4  Dots chart 5 ft 60");
//            imageList.add(Common.folderImage.get(2));
            /*
            imageList.add(R.drawable._1landoltcsingle8_60);
            imageList.add(R.drawable._2landoltcsingle8_36);
            imageList.add(R.drawable._3landoltcsingle_8_24);
            imageList.add(R.drawable._4landoltcsingl8_18);
            imageList.add(R.drawable._5landoltsingle8_12);
            imageList.add(R.drawable._6landoltsingl8_9);
            imageList.add(R.drawable._7landoltsingl8_6);*/
        } else if (screen.equals("4") && preferenceManager.getDistances().equals("5")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
//            imageList.add(Common.folderImage.get(3));
            imageList.add(R.drawable._1_duochrome5ft200);
            imageList.add(R.drawable._2_duochrome5ft120);
            imageList.add(R.drawable._3_duochrome5ft80);
            imageList.add(R.drawable._4_duochrome5ft60);
            imageList.add(R.drawable._5_duochrome5ft40);
            imageList.add(R.drawable._6_duochrome5ft30);
            imageList.add(R.drawable._7_duochrome5ft20);
            imageList.add(R.drawable._8_duochrome5ft17);

            imageName.add("1  Duochrome 5 ft 200");
            imageName.add("2  Duochrome 5 ft 120");
            imageName.add("3  Duochrome 5 ft 80");
            imageName.add("4  Duochrome 5 ft 60");
            imageName.add("5  Duochrome 5 ft 40");
            imageName.add("6  Duochrome 5 ft 30");
            imageName.add("7  Duochrome 5 ft 20");
            imageName.add("8  Duochrome 5 ft 17");
        } else if (screen.equals("5") && preferenceManager.getDistances().equals("5")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();

            imageList.add(R.drawable._1duochrome5ft200);
            imageList.add(R.drawable._2duochrome5ft120);
            imageList.add(R.drawable._3duochrome5ft80);
            imageList.add(R.drawable._4duochrome5f60);
            imageList.add(R.drawable._5duochrome5ft40);
            imageList.add(R.drawable._6duochrome5ft30);
            imageList.add(R.drawable._7duochrome5ft20);
            imageList.add(R.drawable._8duochrome5ft17);

            imageName.add("1  Duochrome 5 ft 200");
            imageName.add("2  Duochrome 5 ft 120");
            imageName.add("3  Duochrome 5 ft 80");
            imageName.add("4  Duochrome 5 ft 60");
            imageName.add("5  Duochrome 5 ft 40");
            imageName.add("6  Duochrome 5 ft 30");
            imageName.add("7  Duochrome 5 ft 20");
            imageName.add("8  Duochrome 5 ft 17");
        }else if (screen.equals("6") && preferenceManager.getDistances().equals("5")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartlogmar5ft);
            imageName.add("Equity Chart- Logmar  5 FT");
        }else if (screen.equals("7") && preferenceManager.getDistances().equals("5")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev015ft);
            imageList.add(R.drawable._2numberrev005ft60);
            imageList.add(R.drawable._3numberrev005ft36);
            imageList.add(R.drawable._4numberrev5ft24);
            imageList.add(R.drawable._5numberrev5ft18);
            imageList.add(R.drawable._6numberrev5ft12);
            imageList.add(R.drawable._7numberrev5ft9);
            imageList.add(R.drawable._8numberrev005ft6);
            imageName.add("1  number Rev 01  5 FT");
            imageName.add("2  number Rev 00  5 FT 60");
            imageName.add("3  number Rev 00  5 FT 36");
            imageName.add("4  number Rev 00  5 FT 24");
            imageName.add("5  number Rev 00  5 FT 18");
            imageName.add("6  number Rev 00  5 FT 12");
            imageName.add("7  number Rev 00  5 FT 9");
            imageName.add("8  number Rev 00  5 FT 6");
        }else if (screen.equals("8") && preferenceManager.getDistances().equals("5")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();


        }else if (screen.equals("9") && preferenceManager.getDistances().equals("5")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1echartrev005ftrev01);
            imageList.add(R.drawable._2echartrev005ftrev60);
            imageList.add(R.drawable._3echartrev005ftrev36);
            imageList.add(R.drawable._4echartrev005ftrev24);
            imageList.add(R.drawable._5echartrev005ftrev18);
            imageList.add(R.drawable._6echartrev005ftrev12);
            imageList.add(R.drawable._7echartrev005ftrev9);
            imageList.add(R.drawable._8echartrev005ftrev6);
            imageName.add("1  E chart Rev 00  5 FT Rev 01");
            imageName.add("2 E chart Rev 00  5 FT 60");
            imageName.add("3 E chart Rev 00  5 FT 36");
            imageName.add("4  E chart Rev 00  5 FT 24");
            imageName.add("5  E chart Rev 00  5 FT 18");
            imageName.add("6  E chart Rev 00  5 FT 12");
            imageName.add("7  E chart Rev 00  5 FT 9");
            imageName.add("8  E chart Rev 00  5 FT 6");

        }else if (screen.equals("1") && preferenceManager.getDistances().equals("6")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();

            imageList.add(R.drawable._1equity_chartenglishrev_6ft);
            imageList.add(R.drawable._2equity_chartenglishrev_6ft);
            imageList.add(R.drawable._3equity_chartenglishrev_6ft);
            imageList.add(R.drawable._4equity_chartenglishrev_6ft);
            imageList.add(R.drawable._5equity_chartenglishrev_6ft);
            imageList.add(R.drawable._6equity_chartenglishrev_6ft);
            imageList.add(R.drawable._7equity_chartenglishrev_6ft);
            imageList.add(R.drawable._8equity_chartenglishrev_6ft);
            imageName.add("1 Equity Chart- ENGLISH Rev 01  6 FT");
            imageName.add("2  Equity Chart- ENGLISH Rev 00  6 FT 60");
            imageName.add("3  Equity Chart- ENGLISH Rev 00  6 FT 36");
            imageName.add("4  Equity Chart- ENGLISH Rev 00  6 FT 24");
            imageName.add("5  Equity Chart- ENGLISH Rev 00  6 FT 18");
            imageName.add("6  Equity Chart- ENGLISH Rev 00  6 FT 12");
            imageName.add("7  Equity Chart- ENGLISH Rev 00  6 FT 9");
            imageName.add("8  Equity Chart- ENGLISH Rev 00  6 FT 6");

        }else if (screen.equals("2") && preferenceManager.getDistances().equals("6")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschart_6ft200);
            imageList.add(R.drawable._2dotschart_6ft200);
            imageList.add(R.drawable._3dotschart_6ft200);
            imageList.add(R.drawable._4dotschart_6ft200);
            imageName.add("1  Dots chart 6 ft 200");
            imageName.add("2  Dots chart 6 ft 120");
            imageName.add("3  Dots chart 6 ft 80");
            imageName.add("4  Dots chart 6 ft 60");

        }else if (screen.equals("3") && preferenceManager.getDistances().equals("6")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();

            imageList.add(R.drawable._1duochrome_6ft200);
            imageList.add(R.drawable._2duochrome_6ft);
            imageList.add(R.drawable._3duochrome_6ft);
            imageList.add(R.drawable._4duochrome_6ft);
            imageList.add(R.drawable._5duochrome_6ft);
            imageList.add(R.drawable._6duochrome_6ft);
            imageList.add(R.drawable._7duochrome_6ft);
            imageName.add("1  Duochrome 6 ft 200");
            imageName.add("2  Duochrome 6 ft 120");
            imageName.add("3  Duochrome 6 ft 80");
            imageName.add("4  Duochrome 6 ft 60");
            imageName.add("5  Duochrome 6 ft 40");
            imageName.add("6  Duochrome 6 ft 30");
            imageName.add("7  Duochrome 6 ft 20");

        }else if (screen.equals("4") && preferenceManager.getDistances().equals("6")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome_numeric_6_ft);
            imageList.add(R.drawable._2duochrome_numeric_6_ft);
            imageList.add(R.drawable._3duochrome_numeric_6_ft);
            imageList.add(R.drawable._4duochrome_numeric_6_ft);
            imageList.add(R.drawable._5duochrome_numeric_6_ft);
            imageList.add(R.drawable._6duochrome_numeric_6_ft);
            imageList.add(R.drawable._7duochrome_numeric_6_ft);
            imageName.add("1  Duochrome 6 ft 200");
            imageName.add("2  Duochrome 6 ft 120");
            imageName.add("3  Duochrome 6 ft 80");
            imageName.add("4  Duochrome 6 ft 60");
            imageName.add("5  Duochrome 6 ft 40");
            imageName.add("6  Duochrome 6 ft 30");
            imageName.add("7  Duochrome 6 ft 20");
        }else if (screen.equals("5") && preferenceManager.getDistances().equals("6")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1landoltcsingle_6ft);
            imageList.add(R.drawable._2landoltcsingle_6ft);
            imageList.add(R.drawable._3landoltcsingle_6ft);
            imageList.add(R.drawable._4landoltcsingle_6ft);
            imageList.add(R.drawable._5landoltcsingle_6ft);
            imageList.add(R.drawable._6landoltcsingle_6ft);
            imageList.add(R.drawable._7landoltcsingle_6ft);
            imageName.add("Landolt C single 6-6");
            imageName.add("Landolt C single 6-9 V1");
            imageName.add("Landolt C single 6-12 V1");
            imageName.add("Landolt C single 6-18 v1");
            imageName.add("Landolt C single 6-24");
            imageName.add("Landolt C single 6-36");
            imageName.add("Landolt C single 6-60");

        }else if (screen.equals("6") && preferenceManager.getDistances().equals("6")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartlogma_6ft);
            imageName.add("Equity Chart- Logmar 6 FT");

        }else if (screen.equals("7") && preferenceManager.getDistances().equals("6")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev_6ft);
            imageList.add(R.drawable._2numberrev_6ft);
            imageList.add(R.drawable._3numberrev_6ft);
            imageList.add(R.drawable._4numberrev_6ft);
            imageList.add(R.drawable._5numberrev_6ft);
            imageList.add(R.drawable._6numberrev_6ft);
            imageList.add(R.drawable._7numberrev_6ft);
            imageList.add(R.drawable._8numberrev_6ft);
            imageName.add("1  number Rev 00  6FT");
            imageName.add("2  number Rev 00  6 FT 60");
            imageName.add("3  number Rev 00  6 FT 36");
            imageName.add("4  number Rev 00  6 FT 24");
            imageName.add("5  number Rev 00  6 FT 18");
            imageName.add("6  number Rev 00  6 FT 12");
            imageName.add("7  number Rev 00  6 FT 9");
            imageName.add("8  number Rev 00  6 FT 6");

        }else if (screen.equals("8") && preferenceManager.getDistances().equals("6")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1echartrev_6ft);
            imageList.add(R.drawable._2echartrev_6ft);
            imageList.add(R.drawable._3echartrev_6ft);
            imageList.add(R.drawable._4echartrev_6ft);
            imageList.add(R.drawable._5echartrev_6ft);
            imageList.add(R.drawable._6echartrev_6ft);
            imageList.add(R.drawable._7echartrev_6ft);
            imageList.add(R.drawable._8echartrev_6ft);
            imageName.add("1  E chart Rev 00  6 FT");
            imageName.add("2  E chart Rev 00  6 FT 60");
            imageName.add("3  E chart Rev 00  6 FT 36");
            imageName.add("4  E chart Rev 00  6 FT 24");
            imageName.add("5  E chart Rev 00  6 FT 18");
            imageName.add("6  E chart Rev 00  6 FT 12");
            imageName.add("7  E chart Rev 00  6 FT 9");
            imageName.add("8  E chart Rev 00  6 FT 6");
        }else if (screen.equals("1") && preferenceManager.getDistances().equals("8")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartenglish_rev008ft);
            imageName.add("Equity Chart- ENGLISH Rev 00  8 FT");

        }else if (screen.equals("2") && preferenceManager.getDistances().equals("8")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome8ft200);
            imageList.add(R.drawable._2duochrome8ft120);
            imageList.add(R.drawable._3duochrome8ft80);
            imageList.add(R.drawable._4duochrome8ft60);
            imageList.add(R.drawable._5duochrome8ft40);
            imageList.add(R.drawable._6duochrome8ft30);
            imageList.add(R.drawable._7duochrome8ft20);
            imageName.add("1  Duochrome 8 ft 200");
            imageName.add("2  Duochrome 8 ft 120");
            imageName.add("3  Duochrome 8 ft 80");
            imageName.add("4  Duochrome 8ft 60");
            imageName.add("5  Duochrome 8ft 40");
            imageName.add("6  Duochrome 8 ft 30");
            imageName.add("7  Duochrome 8 ft 20");

        }else if (screen.equals("3") && preferenceManager.getDistances().equals("8")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1landoltcsingle8_60);
            imageList.add(R.drawable._2landoltcsingle8_36);
            imageList.add(R.drawable._3landoltcsingle_8_24);
            imageList.add(R.drawable._4landoltcsingl8_18);
            imageList.add(R.drawable._5landoltsingle8_12);
            imageList.add(R.drawable._6landoltsingl8_9);
            imageList.add(R.drawable._7landoltsingl8_6);
            imageName.add("1  Landolt C single 8-60");
            imageName.add("2  Landolt C single 8-36");
            imageName.add("3  Landolt C single 8-24");
            imageName.add("4  Landolt C single 8-18");
            imageName.add("5  Landolt C single 8-12");
            imageName.add("6  Landolt C single 8-9");
            imageName.add("7  Landolt C single 8-6");

        }else if (screen.equals("4") && preferenceManager.getDistances().equals("8")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartlogmar_8ft);
            imageName.add("Equity Chart- Logmar  8 FT");

        }else if (screen.equals("5") && preferenceManager.getDistances().equals("8")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev00_8ft);
            imageList.add(R.drawable._2numberrev00_8ft);
            imageList.add(R.drawable._3numberrev00_8ft);
            imageList.add(R.drawable._4numberrev00_8ft);
            imageList.add(R.drawable._5numberrev00_8ft);
            imageList.add(R.drawable._6numberrev00_8ft);
            imageList.add(R.drawable._7numberrev00_8ft);
            imageList.add(R.drawable._8numberrev00_8ft);
            imageName.add("1  number Rev 00  8 FT");
            imageName.add("2  number Rev 00  6 FT 60");
            imageName.add("3  number Rev 00  6 FT 36");
            imageName.add("4  number Rev 00  6 FT 24");
            imageName.add("5  number Rev 00  6 FT 18");
            imageName.add("6  number Rev 00  6 FT 12");
            imageName.add("7  number Rev 00  6 FT 9");
            imageName.add("8  number Rev 00  6 FT 6");

        }else if (screen.equals("6") && preferenceManager.getDistances().equals("8")) {
            imageName = new ArrayList<>();
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1echartrev008ft);
            imageName.add("1  E chart Rev 00  8 FT");
        }


        LinearLayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerview.setLayoutManager(layoutManager);
        adapterGallery = new AdapterGallery(imageList, imageName, this);
        recyclerview.setAdapter(adapterGallery);
    }
    @Override
    public void onClick(int position) {
        pos = position;
        fullScreenImage(pos);
    }
    private void fullScreenImage(int position) {
        startActivity(new Intent(Gallery.this, FullImageActivity.class)
                .putExtra("pos", position)
                .putExtra("screen", getIntent().getStringExtra("screen")));
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
                if (imageList.size() >= 0) {
                    fullScreenImage(0);
                }
                break;
            case KeyEvent.KEYCODE_2:
                if (imageList.size() >= 1) {
                    fullScreenImage(1);
                }
                break;
            case KeyEvent.KEYCODE_3:
                if (imageList.size() >= 2) {
                    fullScreenImage(2);
                }
                break;
            case KeyEvent.KEYCODE_4:
                if (imageList.size() >= 3) {
                    fullScreenImage(3);
                }
                break;
            case KeyEvent.KEYCODE_5:
                if (imageList.size() >= 4) {
                    fullScreenImage(4);
                } else {
                }
                break;
            case KeyEvent.KEYCODE_6:
                if (imageList.size() >= 5) {
                    fullScreenImage(5);
                } else {
                }
                break;
            case KeyEvent.KEYCODE_7:
                if (imageList.size() >= 6) {
                    fullScreenImage(6);
                } else {
                }
                break;
            case KeyEvent.KEYCODE_8:
                if (imageList.size() >= 7) {
                    fullScreenImage(7);
                } else {
                }
                break;
            case KeyEvent.KEYCODE_9:
                if (imageList.size() >= 8) {
                    fullScreenImage(8);
                } else {
                }
                break;
            case KeyEvent.KEYCODE_ENTER:
                if (imageList.size() > 0) {
                    fullScreenImage(0);
                } else {
                    Toast.makeText(this, "This is first image", Toast.LENGTH_SHORT).show();
                }
                break;
            case KeyEvent.KEYCODE_BACK:
                finish();
                break;
        }
        return true;
    }
}
