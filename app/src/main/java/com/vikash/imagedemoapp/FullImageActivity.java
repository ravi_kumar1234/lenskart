package com.vikash.imagedemoapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.vikash.imagedemoapp.common.Common;
import com.vikash.imagedemoapp.common.PreferenceManager;
import java.util.ArrayList;

public class FullImageActivity extends AppCompatActivity {
    private ImageView imageView;
    private ArrayList<Integer> imageList;
    int pos=0;
    private Button buttonNext,buttonBack;
    private String screen="0";
    private PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Common.fullScreencall(getWindow(), this);
        Common.hideKeyboard(this);
        setContentView(R.layout.activity_full_image);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        screen = getIntent().getStringExtra("screen");
        preferenceManager = new PreferenceManager(this);
        imageView = findViewById(R.id.imageView);
       // buttonNext = findViewById(R.id.buttonNext);
       // buttonBack = findViewById(R.id.buttonBack);
        imageList = new ArrayList<Integer>();
        pos = getIntent().getIntExtra("pos",0);
        if (screen.equals("0") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitychart_english_rev005ft);
            imageList.add(R.drawable._2equitychart_english_rev005ft60);
            imageList.add(R.drawable._3equitychart_english_rev005ft36);
            imageList.add(R.drawable._4equitychart_english_rev005ft24);
            imageList.add(R.drawable._5equitychart_english_rev005ft18);
            imageList.add(R.drawable._6equitychart_english_rev005ft12);
            imageList.add(R.drawable._7equitychart_english_rev005ft9);
            imageList.add(R.drawable._8equitychart_english_rev005ft6);
            imageList.add(R.drawable._9equitychart_english_rev005ft5);
            imageList.add(R.drawable._10equitychart_english_rev005ft);
            imageList.add(R.drawable._11equitychart_english_rev005ft60);
            imageList.add(R.drawable._12equitychart_english_rev005ft36);
            imageList.add(R.drawable._13equitychart_english_rev005ft24);
            imageList.add(R.drawable._14equitychart_english_rev005ft18);
            imageList.add(R.drawable._15equitychart_english_rev005ft12);
            imageList.add(R.drawable._16equitychart_english_rev005ft9);
            imageList.add(R.drawable._17equitychart_english_rev005ft6);
            imageList.add(R.drawable._18equitychart_english_rev005ft5);
        } else if (screen.equals("1") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
//            imageList.add(Common.folderImage.get(1));
            imageList.add(R.drawable._1landoltc_5ftsingle660);
            imageList.add(R.drawable._2landoltc_5ftsingle636);
            imageList.add(R.drawable._3landoltc_5ftsingle624);
            imageList.add(R.drawable._4landoltc_5ftsingle618);
            imageList.add(R.drawable._5landoltc_5ftsingle612);
            imageList.add(R.drawable._6landoltc_5ftsingle69);
            imageList.add(R.drawable._7landoltc_5ftsingle66);
            imageList.add(R.drawable._8landoltc_5ftsingle65);
        }
        else if (screen.equals("2") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschar5ft200);
            imageList.add(R.drawable._2dotschart5ft120);
            imageList.add(R.drawable._3dotschart5ft80);
            imageList.add(R.drawable._4dotschart5f60);
            imageList.add(R.drawable._5dotschart5f40);
            imageList.add(R.drawable._6dotschart5f30);
            imageList.add(R.drawable._7dotschart5f20);
            imageList.add(R.drawable._8dotschart5f17);
  //         imageList.add(Common.folderImage.get(2));

        } else if (screen.equals("3") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
//            imageList.add(Common.folderImage.get(3));
            imageList.add(R.drawable._1_duochrome5ft200);
            imageList.add(R.drawable._2_duochrome5ft120);
            imageList.add(R.drawable._3_duochrome5ft80);
            imageList.add(R.drawable._4_duochrome5ft60);
            imageList.add(R.drawable._5_duochrome5ft40);
            imageList.add(R.drawable._6_duochrome5ft30);
            imageList.add(R.drawable._7_duochrome5ft20);
            imageList.add(R.drawable._8_duochrome5ft17);
} else if (screen.equals("4") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome5ft200);
            imageList.add(R.drawable._2duochrome5ft120);
            imageList.add(R.drawable._3duochrome5ft80);
            imageList.add(R.drawable._4duochrome5f60);
            imageList.add(R.drawable._5duochrome5ft40);
            imageList.add(R.drawable._6duochrome5ft30);
            imageList.add(R.drawable._7duochrome5ft20);
            imageList.add(R.drawable._8duochrome5ft17);
        }else if (screen.equals("5") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartlogmar5ft);
        }else if (screen.equals("6") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev015ft);
            imageList.add(R.drawable._2numberrev005ft60);
            imageList.add(R.drawable._3numberrev005ft36);
            imageList.add(R.drawable._4numberrev5ft24);
            imageList.add(R.drawable._5numberrev5ft18);
            imageList.add(R.drawable._6numberrev5ft12);
            imageList.add(R.drawable._7numberrev5ft9);
            imageList.add(R.drawable._8numberrev005ft6);
            imageList.add(R.drawable._9numberrev005ft5);
            imageList.add(R.drawable._10numberrev015ft);
            imageList.add(R.drawable._11numberrev005ft60);
            imageList.add(R.drawable._12numberrev005ft36);
            imageList.add(R.drawable._13numberrev5ft24);
            imageList.add(R.drawable._14numberrev5ft18);
            imageList.add(R.drawable._15numberrev5ft12);
            imageList.add(R.drawable._16numberrev5ft9);
            imageList.add(R.drawable._17numberrev005ft6);
            imageList.add(R.drawable._18numberrev005ft5);

        }else if (screen.equals("7") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitycharthindi5ft60);
            imageList.add(R.drawable._2equitycharthindi5ft36);
            imageList.add(R.drawable._3equitycharthindi5ft24);
            imageList.add(R.drawable._4equitycharthindi5ft18);
            imageList.add(R.drawable._5equitycharthindi5ft12);
            imageList.add(R.drawable._6equitycharthindi5ft9);
            imageList.add(R.drawable._7equitycharthindi5ft6);
            imageList.add(R.drawable._8equitycharthindi5ft5);
        }else if (screen.equals("8") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1echartrev005ftrev01);
            imageList.add(R.drawable._2echartrev005ftrev60);
            imageList.add(R.drawable._3echartrev005ftrev36);
            imageList.add(R.drawable._4echartrev005ftrev24);
            imageList.add(R.drawable._5echartrev005ftrev18);
            imageList.add(R.drawable._6echartrev005ftrev12);
            imageList.add(R.drawable._7echartrev005ftrev9);
            imageList.add(R.drawable._8echartrev005ftrev6);
            imageList.add(R.drawable._9echartrev005ftrev5);
        }
        else if (screen.equals("9") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome5fttumbe200);
            imageList.add(R.drawable._2_duochrome5fttumbe120);
            imageList.add(R.drawable._3_duochrome5fttumbe80);
            imageList.add(R.drawable._4_duochrome5fttumbe60);
            imageList.add(R.drawable._5_duochrome5fttumbe40);
            imageList.add(R.drawable._6_duochrome5fttumbe30);
            imageList.add(R.drawable._7_duochrome5fttumbe20);
            imageList.add(R.drawable._8_duochrome5fttumbe17);
        }
        else if (screen.equals("10") && preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.isihara_1_5);
            imageList.add(R.drawable.isihara_2_5);
            imageList.add(R.drawable.isihara_3_5);
            imageList.add(R.drawable.isihara_4_5);
            imageList.add(R.drawable.isihara_5_5);
            imageList.add(R.drawable.isihara_6_5);
            imageList.add(R.drawable.isihara_7_5);
            imageList.add(R.drawable.isihara_8_5);
            imageList.add(R.drawable.isihara_9_5);
            imageList.add(R.drawable.isihara_10_5);
            imageList.add(R.drawable.isihara_11_5);
        }
        else if (screen.equals("0") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equity_chartenglishrev_6ft);
            imageList.add(R.drawable._2equity_chartenglishrev_6ft);
            imageList.add(R.drawable._3equity_chartenglishrev_6ft);
            imageList.add(R.drawable._4equity_chartenglishrev_6ft);
            imageList.add(R.drawable._5equity_chartenglishrev_6ft);
            imageList.add(R.drawable._6equity_chartenglishrev_6ft);
            imageList.add(R.drawable._7equity_chartenglishrev_6ft);
            imageList.add(R.drawable._8equity_chartenglishrev_6ft);
            imageList.add(R.drawable._9equity_chartenglishrev_6ft);
            imageList.add(R.drawable._10equity_chartenglishrev_6ft);
            imageList.add(R.drawable._11equity_chartenglishrev_6ft);
            imageList.add(R.drawable._12equity_chartenglishrev_6ft);
            imageList.add(R.drawable._13equity_chartenglishrev_6ft);
            imageList.add(R.drawable._14equity_chartenglishrev_6ft);
            imageList.add(R.drawable._15equity_chartenglishrev_6ft);
            imageList.add(R.drawable._16equity_chartenglishrev_6ft);
            imageList.add(R.drawable._17equity_chartenglishrev_6ft);
            imageList.add(R.drawable._18equity_chartenglishrev_6ft);
        }else if (screen.equals("1") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._7landoltcsingle_6ft);
            imageList.add(R.drawable._6landoltcsingle_6ft);
            imageList.add(R.drawable._5landoltcsingle_6ft);
            imageList.add(R.drawable._4landoltcsingle_6ft);
            imageList.add(R.drawable._3landoltcsingle_6ft);
            imageList.add(R.drawable._2landoltcsingle_6ft);
            imageList.add(R.drawable._1landoltcsingle_6ft);
            imageList.add(R.drawable._8landoltcsingle_6ft);

        }else if (screen.equals("2") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschart_6ft200);
            imageList.add(R.drawable._2dotschart_6ft200);
            imageList.add(R.drawable._3dotschart_6ft200);
            imageList.add(R.drawable._4dotschart_6ft200);
            imageList.add(R.drawable._5dotschart_6ft40);
            imageList.add(R.drawable._6dotschart_6ft30);
            imageList.add(R.drawable._7dotschart_6ft20);
            imageList.add(R.drawable._8dotschart_6ft17);

        }else if (screen.equals("3") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome_6ft200);
            imageList.add(R.drawable._2duochrome_6ft);
            imageList.add(R.drawable._3duochrome_6ft);
            imageList.add(R.drawable._4duochrome_6ft);
            imageList.add(R.drawable._5duochrome_6ft);
            imageList.add(R.drawable._6duochrome_6ft);
            imageList.add(R.drawable._7duochrome_6ft);
            imageList.add(R.drawable._8duochrome_6ft);
        }else if (screen.equals("4") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome_numeric_6_ft);
            imageList.add(R.drawable._2duochrome_numeric_6_ft);
            imageList.add(R.drawable._3duochrome_numeric_6_ft);
            imageList.add(R.drawable._4duochrome_numeric_6_ft);
            imageList.add(R.drawable._5duochrome_numeric_6_ft);
            imageList.add(R.drawable._6duochrome_numeric_6_ft);
            imageList.add(R.drawable._7duochrome_numeric_6_ft);
            imageList.add(R.drawable._8duochrome_numeric_6_ft);
        }else if (screen.equals("5") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartlogma_6ft);

        }else if (screen.equals("6") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev_6ft);
            imageList.add(R.drawable._2numberrev_6ft);
            imageList.add(R.drawable._3numberrev_6ft);
            imageList.add(R.drawable._4numberrev_6ft);
            imageList.add(R.drawable._5numberrev_6ft);
            imageList.add(R.drawable._6numberrev_6ft);
            imageList.add(R.drawable._7numberrev_6ft);
            imageList.add(R.drawable._8numberrev_6ft);
            imageList.add(R.drawable._9numberrev_6ft);
            imageList.add(R.drawable._10numberrev_6ft);
            imageList.add(R.drawable._11numberrev_6ft);
            imageList.add(R.drawable._12numberrev_6ft);
            imageList.add(R.drawable._13numberrev_6ft);
            imageList.add(R.drawable._14numberrev_6ft);
            imageList.add(R.drawable._15numberrev_6ft);
            imageList.add(R.drawable._16numberrev_6ft);
            imageList.add(R.drawable._17numberrev_6ft);
            imageList.add(R.drawable._18numberrev_6ft);

        }else if (screen.equals("7") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitycharthindi6ft60);
            imageList.add(R.drawable._2equitycharthindi6ft36);
            imageList.add(R.drawable._3equitycharthindi6ft24);
            imageList.add(R.drawable._4equitycharthindi6ft18);
            imageList.add(R.drawable._5equitycharthindi6ft12);
            imageList.add(R.drawable._6equitycharthindi6ft9);
            imageList.add(R.drawable._7equitycharthindi6ft6);
            imageList.add(R.drawable._8equitycharthindi6ft5);
        }
        else if (screen.equals("8") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1echartrev_6ft);
            imageList.add(R.drawable._2echartrev_6ft);
            imageList.add(R.drawable._3echartrev_6ft);
            imageList.add(R.drawable._4echartrev_6ft);
            imageList.add(R.drawable._5echartrev_6ft);
            imageList.add(R.drawable._6echartrev_6ft);
            imageList.add(R.drawable._7echartrev_6ft);
            imageList.add(R.drawable._8echartrev_6ft);
            imageList.add(R.drawable._9echartrev_6ft);
        }
        else if (screen.equals("9") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome6fttumblinee200);
            imageList.add(R.drawable._2_duochrome6fttumblinee120);
            imageList.add(R.drawable._3_duochrome6fttumblinee80);
            imageList.add(R.drawable._4_duochrome6fttumblinee60);
            imageList.add(R.drawable._5_duochrome6fttumblinee40);
            imageList.add(R.drawable._6_duochrome6fttumblinee30);
            imageList.add(R.drawable._7_duochrome6fttumblinee20);
            imageList.add(R.drawable._8_duochrome6fttumblinee17);
        }
        else if (screen.equals("10") && preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.isihara_common_1);
            imageList.add(R.drawable.isihara_common_2);
            imageList.add(R.drawable.isihara_common_3);
            imageList.add(R.drawable.isihara_common_4);
            imageList.add(R.drawable.isihara_common_5);
            imageList.add(R.drawable.isihara_common_6);
            imageList.add(R.drawable.isihara_common_7);
            imageList.add(R.drawable.isihara_common_8);
            imageList.add(R.drawable.isihara_common_9);
            imageList.add(R.drawable.isihara_common_10);
            imageList.add(R.drawable.isihara_common_11);
        }
        else if (screen.equals("0") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartenglish_rev008ft);
            imageList.add(R.drawable._2equitychart_english_rev008ft60);
            imageList.add(R.drawable._3equitychart_english_rev008ft36);
            imageList.add(R.drawable._4equitychart_english_rev008ft24);
            imageList.add(R.drawable._5equitychart_english_rev008ft18);
            imageList.add(R.drawable._6equitychart_english_rev008ft12);
            imageList.add(R.drawable._7equitychart_english_rev008ft9);
            imageList.add(R.drawable._8equitychart_english_rev008ft6);
            imageList.add(R.drawable._9equitychart_english_rev008ft5);
            imageList.add(R.drawable._10equitychartenglish_rev008ft);
            imageList.add(R.drawable._11equitychart_english_rev008ft60);
            imageList.add(R.drawable._12equitychart_english_rev008ft36);
            imageList.add(R.drawable._13equitychart_english_rev008ft24);
            imageList.add(R.drawable._14equitychart_english_rev008ft18);
            imageList.add(R.drawable._15equitychart_english_rev008ft12);
            imageList.add(R.drawable._16equitychart_english_rev008ft9);
            imageList.add(R.drawable._17equitychart_english_rev008ft6);
            imageList.add(R.drawable._18equitychart_english_rev008ft5);

        }else if (screen.equals("1") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1landoltcsingle8_60);
            imageList.add(R.drawable._2landoltcsingle8_36);
            imageList.add(R.drawable._3landoltcsingle_8_24);
            imageList.add(R.drawable._4landoltcsingl8_18);
            imageList.add(R.drawable._5landoltsingle8_12);
            imageList.add(R.drawable._6landoltsingl8_9);
            imageList.add(R.drawable._7landoltsingl8_6);
            imageList.add(R.drawable._8landoltsingl8_5);

        }else if (screen.equals("2") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschart8ft200);
            imageList.add(R.drawable._2dotschart8ft120);
            imageList.add(R.drawable._3dotschart8ft80);
            imageList.add(R.drawable._4dotschart8ft60);
            imageList.add(R.drawable._5dotschart8ft40);
            imageList.add(R.drawable._6dotschart8ft30);
            imageList.add(R.drawable._7dotschart8ft20);
            imageList.add(R.drawable._8dotschart8ft17);
        }else if (screen.equals("3") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome8ft200);
            imageList.add(R.drawable._2duochrome8ft120);
            imageList.add(R.drawable._3duochrome8ft80);
            imageList.add(R.drawable._4duochrome8ft60);
            imageList.add(R.drawable._5duochrome8ft40);
            imageList.add(R.drawable._6duochrome8ft30);
            imageList.add(R.drawable._7duochrome8ft20);
            imageList.add(R.drawable._8duochrome8ft17);
            /*imageList.add(R.drawable.equitychartlogmar_8ft);*/

        }else if (screen.equals("4") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome8ft200);
            imageList.add(R.drawable._2_duochrome8ft120);
            imageList.add(R.drawable._3_duochrome8ft80);
            imageList.add(R.drawable._4_duochrome8ft60);
            imageList.add(R.drawable._5_duochrome8ft40);
            imageList.add(R.drawable._6_duochrome8ft30);
            imageList.add(R.drawable._7_duochrome8ft20);
            imageList.add(R.drawable._8_duochrome8ft17);
        }
        else if (screen.equals("5") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.equitychartlogmar_8ft);
         }
        else if (screen.equals("6") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev00_8ft);
            imageList.add(R.drawable._2numberrev00_8ft);
            imageList.add(R.drawable._3numberrev00_8ft);
            imageList.add(R.drawable._4numberrev00_8ft);
            imageList.add(R.drawable._5numberrev00_8ft);
            imageList.add(R.drawable._6numberrev00_8ft);
            imageList.add(R.drawable._7numberrev00_8ft);
            imageList.add(R.drawable._8numberrev00_8ft);
            imageList.add(R.drawable._9numberrev00_8ft);
            imageList.add(R.drawable._10numberrev00_8ft);
            imageList.add(R.drawable._11numberrev00_8ft);
            imageList.add(R.drawable._12numberrev00_8ft);
            imageList.add(R.drawable._13numberrev00_8ft);
            imageList.add(R.drawable._14numberrev00_8ft);
            imageList.add(R.drawable._15numberrev00_8ft);
            imageList.add(R.drawable._16numberrev00_8ft);
            imageList.add(R.drawable._17numberrev00_8ft);
            imageList.add(R.drawable._18numberrev00_8ft);
        }
        else if (screen.equals("7") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitycharthindi8ft60);
            imageList.add(R.drawable._2euitycharthindi8ft36);
            imageList.add(R.drawable._3equitycharthindi8ft24);
            imageList.add(R.drawable._4equitycharthindi8ft16);
            imageList.add(R.drawable._5equitycharthindi8ft12);
            imageList.add(R.drawable._6equitycharthindi8ft9);
            imageList.add(R.drawable._7equitycharthindi8ft6);
            imageList.add(R.drawable._8equitycharthindi8ft5);
        }
        else if (screen.equals("8") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1echartrev008ft);
            imageList.add(R.drawable._2echartrev008ftrev60);
            imageList.add(R.drawable._3echartrev008ftrev36);
            imageList.add(R.drawable._4echartrev008ftrev24);
            imageList.add(R.drawable._5echartrev008ftrev18);
            imageList.add(R.drawable._6echartrev008ftrev12);
            imageList.add(R.drawable._7echartrev008ftrev9);
            imageList.add(R.drawable._8echartrev008ftrev6);
            imageList.add(R.drawable._9echartrev008ftrev5);
        }
        else if (screen.equals("9") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome8fttumblinee200);
            imageList.add(R.drawable._2_duochrome8fttumblinee120);
            imageList.add(R.drawable._3_duochrome8fttumblinee80);
            imageList.add(R.drawable._4_duochrome8fttumblinee60);
            imageList.add(R.drawable._5_duochrome8fttumblinee40);
            imageList.add(R.drawable._6_duochrome8fttumblinee30);
            imageList.add(R.drawable._7_duochrome8fttumblinee20);
            imageList.add(R.drawable._8_duochrome8fttumblinee17);
        }
        else if (screen.equals("10") && preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.isihara_common_1);
            imageList.add(R.drawable.isihara_common_2);
            imageList.add(R.drawable.isihara_common_3);
            imageList.add(R.drawable.isihara_common_4);
            imageList.add(R.drawable.isihara_common_5);
            imageList.add(R.drawable.isihara_common_6);
            imageList.add(R.drawable.isihara_common_7);
            imageList.add(R.drawable.isihara_common_8);
            imageList.add(R.drawable.isihara_common_9);
            imageList.add(R.drawable.isihara_common_10);
            imageList.add(R.drawable.isihara_common_11);
        }
        else if (screen.equals("0") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitychart_rev10ft);
            imageList.add(R.drawable._2equitychart_rev10ft);
            imageList.add(R.drawable._3equitychart_rev10ft);
            imageList.add(R.drawable._4equitychart_rev10ft);
            imageList.add(R.drawable._5equitychart_rev10ft);
            imageList.add(R.drawable._6equitychart_rev10ft);
            imageList.add(R.drawable._7equitychart_rev10ft);
            imageList.add(R.drawable._8equitychart_rev10ft);
            imageList.add(R.drawable._9equitychart_rev10ft);
            imageList.add(R.drawable._10equitychart_rev10ft);
            imageList.add(R.drawable._11equitychart_rev10ft);
            imageList.add(R.drawable._12equitychart_rev10ft);
            imageList.add(R.drawable._13equitychart_rev10ft);
            imageList.add(R.drawable._14equitychart_rev10ft);
            imageList.add(R.drawable._15equitychart_rev10ft);
            imageList.add(R.drawable._16equitychart_rev10ft);
            imageList.add(R.drawable._17equitychart_rev10ft);
            imageList.add(R.drawable._18equitychart_rev10ft);
         }else if (screen.equals("1") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1landoltcsingle10ft);
            imageList.add(R.drawable._2landoltcsingle10ft);
            imageList.add(R.drawable._3landoltcsingle10ft);
            imageList.add(R.drawable._4landoltcsingle10ft);
            imageList.add(R.drawable._5landoltcsingle10ft);
            imageList.add(R.drawable._6landoltcsingle10ft);
            imageList.add(R.drawable._7landoltcsingle10ft);
            imageList.add(R.drawable._8landoltcsingle10ft);

        }else if (screen.equals("2") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1dotschart10ft200);
            imageList.add(R.drawable._2dotschart10ft120);
            imageList.add(R.drawable._3dotschart10ft80);
            imageList.add(R.drawable._4dotschart10ft60);
            imageList.add(R.drawable._5dotschart10ft40);
            imageList.add(R.drawable._6dotschart10ft30);
            imageList.add(R.drawable._7dotschart10ft20);
            imageList.add(R.drawable._8dotschart10ft20);

        }else if (screen.equals("3") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome10ft200);
            imageList.add(R.drawable._2duochrome10ft200);
            imageList.add(R.drawable._3duochrome10ft200);
            imageList.add(R.drawable._4duochrome10ft200);
            imageList.add(R.drawable._5duochrome10ft200);
            imageList.add(R.drawable._6duochrome10ft200);
            imageList.add(R.drawable._7duochrome10ft200);
            imageList.add(R.drawable._8duochrome10ft200);

        }else if (screen.equals("4") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1duochrome10ft);
            imageList.add(R.drawable._2duochrome10ft);
            imageList.add(R.drawable._3duochrome10ft);
            imageList.add(R.drawable._4duochrome10ft);
            imageList.add(R.drawable._5duochrome10ft);
            imageList.add(R.drawable._6duochrome10ft);
            imageList.add(R.drawable._7duochrome10ft);
            imageList.add(R.drawable._8duochrome10ft);

        }else if (screen.equals("5") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitychartlogmar10ft);
        }else if (screen.equals("6") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1numberrev10ft);
            imageList.add(R.drawable._2numberrev10ft);
            imageList.add(R.drawable._3numberrev10ft);
            imageList.add(R.drawable._4numberrev10ft);
            imageList.add(R.drawable._5numberrev10ft);
            imageList.add(R.drawable._6numberrev10ft);
            imageList.add(R.drawable._7numberrev10ft);
            imageList.add(R.drawable._8numberrev10ft);
            imageList.add(R.drawable._9numberrev10ft);
            imageList.add(R.drawable._10numberrev10ft);
            imageList.add(R.drawable._11numberrev10ft);
            imageList.add(R.drawable._12numberrev10ft);
            imageList.add(R.drawable._13numberrev10ft);
            imageList.add(R.drawable._14numberrev10ft);
            imageList.add(R.drawable._15numberrev10ft);
            imageList.add(R.drawable._16numberrev10ft);
            imageList.add(R.drawable._17numberrev10ft);
            imageList.add(R.drawable._18numberrev10ft);

        }else if (screen.equals("7") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1equitycharthindi10ft);
            imageList.add(R.drawable._2_1equitycharthindi10ft);
            imageList.add(R.drawable._3_1equitycharthindi10ft);
            imageList.add(R.drawable._4_1equitycharthindi10ft);
            imageList.add(R.drawable._5_1equitycharthindi10ft);
            imageList.add(R.drawable._6_1equitycharthindi10ft);
            imageList.add(R.drawable._7_1equitycharthindi10ft);
            imageList.add(R.drawable._8_1equitycharthindi10ft);
        }else if (screen.equals("8") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1e_chart_rev10ft);
            imageList.add(R.drawable._2e_chart_rev10ft);
            imageList.add(R.drawable._3e_chart_rev10ft);
            imageList.add(R.drawable._4e_chart_rev10ft);
            imageList.add(R.drawable._5e_chart_rev10ft);
            imageList.add(R.drawable._6e_chart_rev10ft);
            imageList.add(R.drawable._7e_chart_rev10ft);
            imageList.add(R.drawable._8e_chart_rev10ft);
            imageList.add(R.drawable._9e_chart_rev10ft);
        }
        else if (screen.equals("9") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_duochrome10fttumblinee200);
            imageList.add(R.drawable._2_duochrome10fttumblinee120);
            imageList.add(R.drawable._3_duochrome10fttumblinee80);
            imageList.add(R.drawable._4_duochrome10fttumblinee60);
            imageList.add(R.drawable._5_duochrome10fttumblinee40);
            imageList.add(R.drawable._6_duochrome10fttumblinee30);
            imageList.add(R.drawable._7_duochrome10fttumblinee20);
            imageList.add(R.drawable._8_duochrome10fttumblinee20);
        }
        else if (screen.equals("10") && preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable.isihara_common_1);
            imageList.add(R.drawable.isihara_common_2);
            imageList.add(R.drawable.isihara_common_3);
            imageList.add(R.drawable.isihara_common_4);
            imageList.add(R.drawable.isihara_common_5);
            imageList.add(R.drawable.isihara_common_6);
            imageList.add(R.drawable.isihara_common_7);
            imageList.add(R.drawable.isihara_common_8);
            imageList.add(R.drawable.isihara_common_9);
            imageList.add(R.drawable.isihara_common_10);
            imageList.add(R.drawable.isihara_common_11);
        }
        fullScreenImage(pos);

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_RIGHT:
               /* if(imageList.size()>pos+1){
                    fullScreenImage(pos++);
                }else{
//                    Toast.makeText(this, "This is last image", Toast.LENGTH_SHORT).show();
                }*/
                if(imageList.size()>pos+1){
                    //fullScreenImage(pos++);
                    pos++;
                    try{
                      /*  Glide.with(getApplicationContext())
                                .load(imageList.get(pos))
                                .dontAnimate()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder()
                                .into(imageView);*/

                        imageView.setImageResource(imageList.get(pos));
                       /* Picasso.get()
                                .load(imageList.get(pos))
                                .into(imageView);*/
                    }catch (Exception e){}
                }else{
//                    Toast.makeText(this, "This is last image", Toast.LENGTH_SHORT).show();
                }
                break;
            case KeyEvent.KEYCODE_DPAD_UP_RIGHT:
              /*  if(imageList.size()>pos+1){
                    fullScreenImage(pos++);
                }else{
//                    Toast.makeText(this, "This is last image", Toast.LENGTH_SHORT).show();
                }*/
                if(imageList.size()>pos+1){
                    pos++;
                    try{
                       /* Picasso.get()
                                .load(imageList.get(pos))
                                .into(imageView);*/
                       /* Glide.with(getApplicationContext()).
                                load(imageList.get(pos))
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .dontAnimate()
                                .into(imageView);*/
                        imageView.setImageResource(imageList.get(pos));
                    }catch (Exception e){}
                }else{
//                    Toast.makeText(this, "This is last image", Toast.LENGTH_SHORT).show();
                }
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
              /*  if(imageList.size()>=0 && pos>0){
                    fullScreenImage(pos--);

                }else{
//                    Toast.makeText(this, "This is first image", Toast.LENGTH_SHORT).show();
                }*/
              //Ravi changes
                if(imageList.size()>=0 && pos>0){
                    // fullScreenImage(pos--);
                    pos--;
                    try{
                        imageView.setImageResource(imageList.get(pos));
                      //  Glide.with(getApplicationContext()).load(imageList.get(pos)).into(imageView);
                    }catch (Exception e){}

                }else{
                    Intent in=new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(in);
//                    Toast.makeText(this, "This is first image", Toast.LENGTH_SHORT).show();
                }
                break;
            case KeyEvent.KEYCODE_SOFT_LEFT:
              /*  if(imageList.size()>=0 && pos>0){
                    fullScreenImage(pos--);

                }else{
//                    Toast.makeText(this, "This is first image", Toast.LENGTH_SHORT).show();
                }*/
                //Ravi changes
                if(imageList.size()>=0 && pos>0){
                    // fullScreenImage(pos--);
                    pos--;
                    try{
                        imageView.setImageResource(imageList.get(pos));
                        //Glide.with(getApplicationContext()).load(imageList.get(pos)).into(imageView);
                    }catch (Exception e){}

                }else{
                    Intent in=new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(in);
//                    Toast.makeText(this, "This is first image", Toast.LENGTH_SHORT).show();
                }
                return true;
            case KeyEvent.KEYCODE_ENTER:
                if(imageList.size()>=0){
                    fullScreenImage(0);
                }
                break;
            case KeyEvent.KEYCODE_0:
                if(imageList.size()>=0){
                    // fullScreenImage(0);
                    Intent intent = new Intent(FullImageActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("folder",10);
                    intent.putExtras(bundle);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;
            case KeyEvent.KEYCODE_1:
                if(imageList.size()>=0){
                   // fullScreenImage(0);
                    Intent intent = new Intent(FullImageActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("folder", 1);
                    intent.putExtras(bundle);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;
            case KeyEvent.KEYCODE_2:
               /* if(imageList.size()>=1){
                    fullScreenImage(1);
                }*/
                if(imageList.size()>=0){
                    // fullScreenImage(0);
                    Intent intent = new Intent(FullImageActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("folder", 2);
                    intent.putExtras(bundle);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;
            case KeyEvent.KEYCODE_3:
               /* if(imageList.size()>=2){
                    fullScreenImage(2);
                }*/
                if(imageList.size()>=0){
                    // fullScreenImage(0);
                    Intent intent = new Intent(FullImageActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("folder", 3);
                    intent.putExtras(bundle);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;

            case KeyEvent.KEYCODE_4:

                if(imageList.size()>=0){
                    // fullScreenImage(0);
                    Intent intent = new Intent(FullImageActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("folder", 4);
                    intent.putExtras(bundle);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;
            case KeyEvent.KEYCODE_5:
               /* if(imageList.size()>=4){
                    fullScreenImage(4);
                }*/
                if(imageList.size()>=0){
                    // fullScreenImage(0);
                    Intent intent = new Intent(FullImageActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("folder", 5);
                    intent.putExtras(bundle);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;
            case KeyEvent.KEYCODE_6:
                if(imageList.size()>=0){
                    // fullScreenImage(0);
                    Intent intent = new Intent(FullImageActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("folder", 6);
                    intent.putExtras(bundle);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                break;
            case KeyEvent.KEYCODE_7:
                if(imageList.size()>=0){
                    Intent intent = new Intent(FullImageActivity.this, RegionalActivity.class);
                    startActivity(intent);
                    //finish();
                }
                break;
            case KeyEvent.KEYCODE_8:
                if(imageList.size()>=0){
                    Intent intent = new Intent(FullImageActivity.this, MainActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("folder", 8);
                        intent.putExtras(bundle);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                }
                break;
            case KeyEvent.KEYCODE_9:
               /* if(imageList.size()>=8){
                    fullScreenImage(8);
                }*/
                if(imageList.size()>=0){
                    // fullScreenImage(0);
                    if(preferenceManager.getDistances().equals("8")){
                        Toast.makeText(getApplicationContext(),"Please choose number less than 9",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Intent intent = new Intent(FullImageActivity.this, MainActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("folder", 9);
                        intent.putExtras(bundle);
                       // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                }
                break;
            case KeyEvent.KEYCODE_BACK:
               // finish();
                Intent in=new Intent(FullImageActivity.this, MainActivity.class);
               // in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(in);
                finish();
                break;
        }
        return true;
    }
    private void fullScreenImage(int pos) {
        try{
            Glide.with(this).load(imageList.get(pos)).into(imageView);
        }catch (Exception e){}
    }
   /* @Override
    public void onBackPressed(){
        super.onBackPressed();
        onBackPressed();
    }*/
}