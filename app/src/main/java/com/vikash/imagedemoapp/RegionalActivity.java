package com.vikash.imagedemoapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.vikash.imagedemoapp.common.Common;
import com.vikash.imagedemoapp.common.PreferenceManager;
import java.util.ArrayList;

public class RegionalActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout firstLL,secondLL,thirdLL,fourthLL,fifthLL,sixthLL,sevenLL,eightLL;

    private ImageView img1,img2,img3,img4,img5,img6,img7,img8;
    private TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv8;
    private PreferenceManager preferenceManager;
    private String distance = "";
    private ArrayList<Integer> imageList;
    int regioncount=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Common.fullScreencall(getWindow(), this);
        Common.hideKeyboard(this);
        setContentView(R.layout.activity_regional);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        preferenceManager = new PreferenceManager(this);
        initView();
        setIcons();
        distance = preferenceManager.getDistances() + "ft";
        calculateSize();
        if(getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            int folderId = extras.getInt("folder");
            if(folderId==10){
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "0"));
            }
            else if(folderId==1){
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "1"));
            }
        }
        firstLL.setBackgroundResource(R.drawable.round_corner_select_bg);
    }

    private void calculateSize(){
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_bengali);
            imageList.add(R.drawable._2_equity_chart_5ft36_bengali);
            imageList.add(R.drawable._3_equity_chart_5ft24_bengali);
            imageList.add(R.drawable._4_equity_chart_5ft18_bengali);
            imageList.add(R.drawable._5_equity_chart_5ft12_bengali);
            imageList.add(R.drawable._6_equity_chart_5ft9_bengali);
            imageList.add(R.drawable._7_equity_chart_5ft6_bengali);
            tv1.setText("Bengali " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_kannada);
            imageList.add(R.drawable._2_equity_chart_5ft36_kannada);
            imageList.add(R.drawable._3_equity_chart_5ft24_kannada);
            imageList.add(R.drawable._4_equity_chart_5ft18_kannada);
            imageList.add(R.drawable._5_equity_chart_5ft12_kannada);
            imageList.add(R.drawable._6_equity_chart_5ft9_kannada);
            imageList.add(R.drawable._7_equity_chart_5ft6_kannada);
            tv2.setText("Gujrati " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_kannada);
            imageList.add(R.drawable._2_equity_chart_5ft36_kannada);
            imageList.add(R.drawable._3_equity_chart_5ft24_kannada);
            imageList.add(R.drawable._4_equity_chart_5ft18_kannada);
            imageList.add(R.drawable._5_equity_chart_5ft12_kannada);
            imageList.add(R.drawable._6_equity_chart_5ft9_kannada);
            imageList.add(R.drawable._7_equity_chart_5ft6_kannada);
            tv3.setText("Kannada " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_malyalam);
            imageList.add(R.drawable._2_equity_chart_5ft36_malyalam);
            imageList.add(R.drawable._3_equity_chart_5ft24_malyalam);
            imageList.add(R.drawable._4_equity_chart_5ft18_malyalam);
            imageList.add(R.drawable._5_equity_chart_5ft12_malyalam);
            imageList.add(R.drawable._6_equity_chart_5ft9_malyalam);
            imageList.add(R.drawable._7_equity_chart_5ft6_malyalam);
            tv4.setText("Malyalam " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_oodiya);
            imageList.add(R.drawable._2_equity_chart_5ft36_oodiya);
            imageList.add(R.drawable._3_equity_chart_5ft24_oodiya);
            imageList.add(R.drawable._4_equity_chart_5ft18_oodiya);
            imageList.add(R.drawable._5_equity_chart_5ft12_oodiya);
            imageList.add(R.drawable._6_equity_chart_5ft9_oodiya);
            imageList.add(R.drawable._7_equity_chart_5ft6_oodiya);
            tv5.setText("Oodiya " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_punjabi);
            imageList.add(R.drawable._2_equity_chart_5ft36_punjabi);
            imageList.add(R.drawable._3_equity_chart_5ft24_punjabi);
            imageList.add(R.drawable._4_equity_chart_5ft18_punjabi);
            imageList.add(R.drawable._5_equity_chart_5ft12_punjabi);
            imageList.add(R.drawable._6_equity_chart_5ft9_punjabi);
            imageList.add(R.drawable._7_equity_chart_5ft6_punjabi);
            tv6.setText("Punjabi " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_tamil);
            imageList.add(R.drawable._2_equity_chart_5ft36_tamil);
            imageList.add(R.drawable._3_equity_chart_5ft24_tamil);
            imageList.add(R.drawable._4_equity_chart_5ft18_tamil);
            imageList.add(R.drawable._5_equity_chart_5ft12_tamil);
            imageList.add(R.drawable._6_equity_chart_5ft9_tamil);
            imageList.add(R.drawable._7_equity_chart_5ft6_tamil);
            tv7.setText("Tamil " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("5")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_5ft60_telugu);
            imageList.add(R.drawable._2_equity_chart_5ft36_telugu);
            imageList.add(R.drawable._3_equity_chart_5ft24_telugu);
            imageList.add(R.drawable._4_equity_chart_5ft18_telugu);
            imageList.add(R.drawable._5_equity_chart_5ft12_telugu);
            imageList.add(R.drawable._6_equity_chart_5ft9_telugu);
            imageList.add(R.drawable._7_equity_chart_5ft6_telugu);
            tv8.setText("Telugu " + distance+ " ("+imageList.size()+")");
        }

        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_bengali);
            imageList.add(R.drawable._2_equity_chart_6ft36_bengali);
            imageList.add(R.drawable._3_equity_chart_6ft24_bengali);
            imageList.add(R.drawable._4_equity_chart_6ft18_bengali);
            imageList.add(R.drawable._5_equity_chart_6ft12_bengali);
            imageList.add(R.drawable._6_equity_chart_6ft9_bengali);
            imageList.add(R.drawable._7_equity_chart_6ft6_bengali);
            tv1.setText("Bengali " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_gujrati);
            imageList.add(R.drawable._2_equity_chart_6ft36_gujrati);
            imageList.add(R.drawable._3_equity_chart_6ft24_gujrati);
            imageList.add(R.drawable._4_equity_chart_6ft18_gujrati);
            imageList.add(R.drawable._5_equity_chart_6ft12_gujrati);
            imageList.add(R.drawable._6_equity_chart_6ft9_gujrati);
            imageList.add(R.drawable._7_equity_chart_6ft6_gujrati);
            tv2.setText("Gujrati " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_kannada);
            imageList.add(R.drawable._2_equity_chart_6ft36_kannada);
            imageList.add(R.drawable._3_equity_chart_6ft24_kannada);
            imageList.add(R.drawable._4_equity_chart_6ft18_kannada);
            imageList.add(R.drawable._5_equity_chart_6ft12_kannada);
            imageList.add(R.drawable._6_equity_chart_6ft9_kannada);
            imageList.add(R.drawable._7_equity_chart_6ft6_kannada);
            tv3.setText("Kannada " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_malyalam);
            imageList.add(R.drawable._2_equity_chart_6ft36_malyalam);
            imageList.add(R.drawable._3_equity_chart_6ft24_malyalam);
            imageList.add(R.drawable._4_equity_chart_6ft18_malyalam);
            imageList.add(R.drawable._5_equity_chart_6ft12_malyalam);
            imageList.add(R.drawable._6_equity_chart_6ft9_malyalam);
            imageList.add(R.drawable._7_equity_chart_6ft6_malyalam);
            tv4.setText("Malyalam " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_oodiya);
            imageList.add(R.drawable._2_equity_chart_6ft36_oodiya);
            imageList.add(R.drawable._3_equity_chart_6ft24_oodiya);
            imageList.add(R.drawable._4_equity_chart_6ft18_oodiya);
            imageList.add(R.drawable._5_equity_chart_6ft12_oodiya);
            imageList.add(R.drawable._6_equity_chart_6ft9_oodiya);
            imageList.add(R.drawable._7_equity_chart_6ft6_oodiya);
            tv5.setText("Oodiya " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_punjabi);
            imageList.add(R.drawable._2_equity_chart_6ft36_punjabi);
            imageList.add(R.drawable._3_equity_chart_6ft24_punjabi);
            imageList.add(R.drawable._4_equity_chart_6ft18_punjabi);
            imageList.add(R.drawable._5_equity_chart_6ft12_punjabi);
            imageList.add(R.drawable._6_equity_chart_6ft9_punjabi);
            imageList.add(R.drawable._7_equity_chart_6ft6_punjabi);
            tv6.setText("Punjabi " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_tamil);
            imageList.add(R.drawable._2_equity_chart_6ft36_tamil);
            imageList.add(R.drawable._3_equity_chart_6ft24_tamil);
            imageList.add(R.drawable._4_equity_chart_6ft18_tamil);
            imageList.add(R.drawable._5_equity_chart_6ft12_tamil);
            imageList.add(R.drawable._6_equity_chart_6ft9_tamil);
            imageList.add(R.drawable._7_equity_chart_6ft6_tamil);
            tv7.setText("Tamil " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("6")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_6ft60_telugu);
            imageList.add(R.drawable._2_equity_chart_6ft36_telugu);
            imageList.add(R.drawable._3_equity_chart_6ft24_telugu);
            imageList.add(R.drawable._4_equity_chart_6ft18_telugu);
            imageList.add(R.drawable._5_equity_chart_6ft12_telugu);
            imageList.add(R.drawable._6_equity_chart_6ft9_telugu);
            imageList.add(R.drawable._7_equity_chart_6ft6_telugu);
            tv8.setText("Telugu " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_bengali);
            imageList.add(R.drawable._2_equity_chart_8ft36_bengali);
            imageList.add(R.drawable._3_equity_chart_8ft24_bengali);
            imageList.add(R.drawable._4_equity_chart_8ft18_bengali);
            imageList.add(R.drawable._5_equity_chart_8ft12_bengali);
            imageList.add(R.drawable._6_equity_chart_8ft9_bengali);
            imageList.add(R.drawable._7_equity_chart_8ft6_bengali);
            tv1.setText("Bengali " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_gujrati);
            imageList.add(R.drawable._2_equity_chart_8ft36_gujrati);
            imageList.add(R.drawable._3_equity_chart_8ft24_gujrati);
            imageList.add(R.drawable._4_equity_chart_8ft18_gujrati);
            imageList.add(R.drawable._5_equity_chart_8ft12_gujrati);
            imageList.add(R.drawable._6_equity_chart_8ft9_gujrati);
            imageList.add(R.drawable._7_equity_chart_8ft6_gujrati);
            tv2.setText("Gujrati " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_kannada);
            imageList.add(R.drawable._2_equity_chart_8ft36_kannada);
            imageList.add(R.drawable._3_equity_chart_8ft24_kannada);
            imageList.add(R.drawable._4_equity_chart_8ft18_kannada);
            imageList.add(R.drawable._5_equity_chart_8ft12_kannada);
            imageList.add(R.drawable._6_equity_chart_8ft9_kannada);
            imageList.add(R.drawable._7_equity_chart_8ft6_kannada);
            tv3.setText("Kannada " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_malyalam);
            imageList.add(R.drawable._2_equity_chart_8ft36_malyalam);
            imageList.add(R.drawable._3_equity_chart_8ft24_malyalam);
            imageList.add(R.drawable._4_equity_chart_8ft18_malyalam);
            imageList.add(R.drawable._5_equity_chart_8ft12_malyalam);
            imageList.add(R.drawable._6_equity_chart_8ft9_malyalam);
            imageList.add(R.drawable._7_equity_chart_8ft6_malyalam);
            tv4.setText("Malyalam " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_oodiya);
            imageList.add(R.drawable._2_equity_chart_8ft36_oodiya);
            imageList.add(R.drawable._3_equity_chart_8ft24_oodiya);
            imageList.add(R.drawable._4_equity_chart_8ft18_oodiya);
            imageList.add(R.drawable._5_equity_chart_8ft12_oodiya);
            imageList.add(R.drawable._6_equity_chart_8ft9_oodiya);
            imageList.add(R.drawable._7_equity_chart_8ft6_oodiya);
            tv5.setText("Oodiya " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_punjabi);
            imageList.add(R.drawable._2_equity_chart_8ft36_punjabi);
            imageList.add(R.drawable._3_equity_chart_8ft24_punjabi);
            imageList.add(R.drawable._4_equity_chart_8ft18_punjabi);
            imageList.add(R.drawable._5_equity_chart_8ft12_punjabi);
            imageList.add(R.drawable._6_equity_chart_8ft9_punjabi);
            imageList.add(R.drawable._7_equity_chart_8ft6_punjabi);
            tv6.setText("Punjabi " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_tamil);
            imageList.add(R.drawable._2_equity_chart_8ft36_tamil);
            imageList.add(R.drawable._3_equity_chart_8ft24_tamil);
            imageList.add(R.drawable._4_equity_chart_8ft18_tamil);
            imageList.add(R.drawable._5_equity_chart_8ft12_tamil);
            imageList.add(R.drawable._6_equity_chart_8ft9_tamil);
            imageList.add(R.drawable._7_equity_chart_8ft6_tamil);
            tv7.setText("Tamil " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("8")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_8ft60_telugu);
            imageList.add(R.drawable._2_equity_chart_8ft36_telugu);
            imageList.add(R.drawable._3_equity_chart_8ft24_telugu);
            imageList.add(R.drawable._4_equity_chart_8ft18_telugu);
            imageList.add(R.drawable._5_equity_chart_8ft12_telugu);
            imageList.add(R.drawable._6_equity_chart_8ft9_telugu);
            imageList.add(R.drawable._7_equity_chart_8ft6_telugu);
            tv8.setText("Telugu " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_bengali);
            imageList.add(R.drawable._2_equity_chart_10ft36_bengali);
            imageList.add(R.drawable._3_equity_chart_10ft24_bengali);
            imageList.add(R.drawable._4_equity_chart_10ft18_bengali);
            imageList.add(R.drawable._5_equity_chart_10ft12_bengali);
            imageList.add(R.drawable._6_equity_chart_10ft9_bengali);
            imageList.add(R.drawable._7_equity_chart_10ft6_bengali);
            tv1.setText("Bengali " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_gujrati);
            imageList.add(R.drawable._2_equity_chart_10ft36_gujrati);
            imageList.add(R.drawable._3_equity_chart_10ft24_gujrati);
            imageList.add(R.drawable._4_equity_chart_10ft18_gujrati);
            imageList.add(R.drawable._5_equity_chart_10ft12_gujrati);
            imageList.add(R.drawable._6_equity_chart_10ft9_gujrati);
            imageList.add(R.drawable._7_equity_chart_10ft6_gujrati);
            tv2.setText("Gujrati " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_kannada);
            imageList.add(R.drawable._2_equity_chart_10ft36_kannada);
            imageList.add(R.drawable._3_equity_chart_10ft24_kannada);
            imageList.add(R.drawable._4_equity_chart_10ft18_kannada);
            imageList.add(R.drawable._5_equity_chart_10ft12_kannada);
            imageList.add(R.drawable._6_equity_chart_10ft9_kannada);
            imageList.add(R.drawable._7_equity_chart_10ft6_kannada);
            tv3.setText("Kannada " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_malyalam);
            imageList.add(R.drawable._2_equity_chart_10ft36_malyalam);
            imageList.add(R.drawable._3_equity_chart_10ft24_malyalam);
            imageList.add(R.drawable._4_equity_chart_10ft18_malyalam);
            imageList.add(R.drawable._5_equity_chart_10ft12_malyalam);
            imageList.add(R.drawable._6_equity_chart_10ft9_malyalam);
            imageList.add(R.drawable._7_equity_chart_10ft6_malyalam);
            tv4.setText("Malyalam " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_oodiya);
            imageList.add(R.drawable._2_equity_chart_10ft36_oodiya);
            imageList.add(R.drawable._3_equity_chart_10ft24_oodiya);
            imageList.add(R.drawable._4_equity_chart_10ft18_oodiya);
            imageList.add(R.drawable._5_equity_chart_10ft12_oodiya);
            imageList.add(R.drawable._6_equity_chart_10ft9_oodiya);
            imageList.add(R.drawable._7_equity_chart_10ft6_oodiya);
            tv5.setText("Oodiya " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_punjabi);
            imageList.add(R.drawable._2_equity_chart_10ft36_punjabi);
            imageList.add(R.drawable._3_equity_chart_10ft24_punjabi);
            imageList.add(R.drawable._4_equity_chart_10ft18_punjabi);
            imageList.add(R.drawable._5_equity_chart_10ft12_punjabi);
            imageList.add(R.drawable._6_equity_chart_10ft9_punjabi);
            imageList.add(R.drawable._7_equity_chart_10ft6_punjabi);
            tv6.setText("Punjabi " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_tamil);
            imageList.add(R.drawable._2_equity_chart_10ft36_tamil);
            imageList.add(R.drawable._3_equity_chart_10ft24_tamil);
            imageList.add(R.drawable._4_equity_chart_10ft18_tamil);
            imageList.add(R.drawable._5_equity_chart_10ft12_tamil);
            imageList.add(R.drawable._6_equity_chart_10ft9_tamil);
            imageList.add(R.drawable._7_equity_chart_10ft6_tamil);
            tv7.setText("Tamil " + distance+ " ("+imageList.size()+")");
        }
        if (preferenceManager.getDistances().equals("10")) {
            imageList = new ArrayList<>();
            imageList.add(R.drawable._1_equity_chart_10ft60_telugu);
            imageList.add(R.drawable._2_equity_chart_10ft36_telugu);
            imageList.add(R.drawable._3_equity_chart_10ft24_telugu);
            imageList.add(R.drawable._4_equity_chart_10ft18_telugu);
            imageList.add(R.drawable._5_equity_chart_10ft12_telugu);
            imageList.add(R.drawable._6_equity_chart_10ft9_telugu);
            imageList.add(R.drawable._7_equity_chart_10ft6_telugu);
            tv8.setText("Telugu " + distance+ " ("+imageList.size()+")");
        }
    }
    private void initView() {
        firstLL=findViewById(R.id.firstLL);
        secondLL=findViewById(R.id.secondLL);
        thirdLL=findViewById(R.id.thirdLL);
        fourthLL=findViewById(R.id.fourthLL);
        fifthLL=findViewById(R.id.fifthLL);
        sixthLL=findViewById(R.id.sixthLL);
        sevenLL=findViewById(R.id.sevenLL);
        eightLL=findViewById(R.id.eightLL);

        img1=findViewById(R.id.img1);
        img2=findViewById(R.id.img2);
        img3=findViewById(R.id.img3);
        img4=findViewById(R.id.img4);
        img5=findViewById(R.id.img5);
        img6=findViewById(R.id.img6);
        img7=findViewById(R.id.img7);
        img8=findViewById(R.id.img8);

        tv1=findViewById(R.id.tv1);
        tv2=findViewById(R.id.tv2);
        tv3=findViewById(R.id.tv3);
        tv4=findViewById(R.id.tv4);
        tv5=findViewById(R.id.tv5);
        tv6=findViewById(R.id.tv6);
        tv7=findViewById(R.id.tv7);
        tv8=findViewById(R.id.tv8);

        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);
        img5.setOnClickListener(this);
        img6.setOnClickListener(this);
        img7.setOnClickListener(this);
        img8.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img1:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).
                        putExtra("screen", "0"));
                break;
            case R.id.img2:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class)
                        .putExtra("screen", "1"));
                break;
            case R.id.img3:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class)
                        .putExtra("screen", "2"));
                break;
            case R.id.img4:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class)
                        .putExtra("screen", "3"));
                break;
            case R.id.img5:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class)
                        .putExtra("screen", "4"));
                break;
            case R.id.img6:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class)
                        .putExtra("screen", "5"));
                break;
            case R.id.img7:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class)
                        .putExtra("screen", "6"));
                break;
            case R.id.img8:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class)
                        .putExtra("screen", "7"));
                break;
        }
    }
    private void setIcons() {
        if (preferenceManager.getDistances().equals("5")) {
            Glide.with(this).load(R.drawable._1_equity_chart_5ft60_bengali)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img1);
            Glide.with(this).load(R.drawable._1_equity_chart_5ft60_gujrati)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img2);
            Glide.with(this).load(R.drawable._1_equity_chart_5ft60_kannada)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img3);
            Glide.with(this).load(R.drawable._1_equity_chart_5ft60_malyalam)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img4);
            Glide.with(this).load(R.drawable._1_equity_chart_5ft60_oodiya)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img5);
            Glide.with(this).load(R.drawable._1_equity_chart_5ft60_punjabi)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img6);
            Glide.with(this).load(R.drawable._1_equity_chart_5ft60_tamil)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img7);
            Glide.with(this).load(R.drawable._1_equity_chart_5ft60_telugu)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img8);
        } else if (preferenceManager.getDistances().equals("6")) {
            Glide.with(this).load(R.drawable._1_equity_chart_6ft60_bengali)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img1);
            Glide.with(this).load(R.drawable._1_equity_chart_6ft60_gujrati)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img2);
            Glide.with(this).load(R.drawable._1_equity_chart_6ft60_kannada)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img3);
            Glide.with(this).load(R.drawable._1_equity_chart_6ft60_malyalam)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img4);
            Glide.with(this).load(R.drawable._1_equity_chart_6ft60_oodiya)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img5);
            Glide.with(this).load(R.drawable._1_equity_chart_6ft60_punjabi)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img6);
            Glide.with(this).load(R.drawable._1_equity_chart_6ft60_tamil)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img7);
            Glide.with(this).load(R.drawable._1_equity_chart_6ft60_telugu)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img8);

        } else if (preferenceManager.getDistances().equals("8")) {
            Glide.with(this).load(R.drawable._1_equity_chart_8ft60_bengali)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img1);
            Glide.with(this).load(R.drawable._1_equity_chart_8ft60_gujrati)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img2);
            Glide.with(this).load(R.drawable._1_equity_chart_8ft60_kannada)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img3);
            Glide.with(this).load(R.drawable._1_equity_chart_8ft60_malyalam)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img4);
            Glide.with(this).load(R.drawable._1_equity_chart_8ft60_oodiya)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img5);
            Glide.with(this).load(R.drawable._1_equity_chart_8ft60_punjabi)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img6);
            Glide.with(this).load(R.drawable._1_equity_chart_8ft60_tamil)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img7);
            Glide.with(this).load(R.drawable._1_equity_chart_8ft60_telugu)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img8);
        }
        else if (preferenceManager.getDistances().equals("10")) {
            Glide.with(this).load(R.drawable._1_equity_chart_10ft60_bengali)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img1);
            Glide.with(this).load(R.drawable._1_equity_chart_10ft60_gujrati)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img2);
            Glide.with(this).load(R.drawable._1_equity_chart_10ft60_kannada)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img3);
            Glide.with(this).load(R.drawable._1_equity_chart_10ft60_malyalam)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img4);
            Glide.with(this).load(R.drawable._1_equity_chart_10ft60_oodiya)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img5);
            Glide.with(this).load(R.drawable._1_equity_chart_10ft60_punjabi)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img6);
            Glide.with(this).load(R.drawable._1_equity_chart_10ft60_tamil)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img7);
            Glide.with(this).load(R.drawable._1_equity_chart_10ft60_telugu)
                    .transform(new CenterCrop(), new RoundedCorners(30)).into(img8);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //onBackPressed();
        startActivity(new Intent(RegionalActivity.this, MainActivity.class));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                //if(preferenceManager.getDistances().equals("5")){
                    if(regioncount<8){
                        regioncount++;
                        Log.e("itemcount", String.valueOf(regioncount));
                        if(regioncount==2){
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(regioncount==3){
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(regioncount==4){
                            fourthLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(regioncount==5){
                            fifthLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(regioncount==6){
                            sixthLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(regioncount==7){
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        else if(regioncount==8){
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                    //}

                }
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                //if(preferenceManager.getDistances().equals("5")) {
                    if (regioncount > 1) {
                        regioncount--;
                        Log.e("itemcount", String.valueOf(regioncount));
                        if (regioncount == 1) {
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                        if (regioncount == 2) {
                            secondLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        } else if (regioncount == 3) {
                            thirdLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        } else if (regioncount == 4) {
                            fourthLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        } else if (regioncount == 5) {
                            fifthLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        } else if (regioncount == 6) {
                            sixthLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        } else if (regioncount == 7) {
                            sevenLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            eightLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        } else if (regioncount == 8) {
                            eightLL.setBackgroundResource(R.drawable.round_corner_select_bg);
                            sevenLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            sixthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fifthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            fourthLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            thirdLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            firstLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                            secondLL.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                  //  }
                }
                break;
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if(regioncount==1){
                    startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "0"));
                }
                else if(regioncount==2){
                    startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "1"));
                }
                else if(regioncount==3){
                    startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "2"));
                }
                else if(regioncount==4){
                    startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "3"));
                }
                else if(regioncount==5){
                    startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "4"));
                }
                else if(regioncount==6){
                    startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "5"));
                }
                else if(regioncount==7){
                    startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "6"));
                }
                else{
                    startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "7"));
                }
                                //startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", String.valueOf(regioncount)));
                break;
            case KeyEvent.KEYCODE_BACK:
                Intent in=new Intent(RegionalActivity.this,MainActivity.class);
                startActivity(in);
                finish();
                break;
            case KeyEvent.KEYCODE_0:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "0"));
                break;
            case KeyEvent.KEYCODE_1:
             startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "1"));
             break;
            case KeyEvent.KEYCODE_2:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "2"));
                break;
            case KeyEvent.KEYCODE_3:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "3"));
                break;
            case KeyEvent.KEYCODE_4:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "4"));
                break;
            case KeyEvent.KEYCODE_5:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "5"));
                break;
            case KeyEvent.KEYCODE_6:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "6"));
                break;
            case KeyEvent.KEYCODE_7:
                startActivity(new Intent(RegionalActivity.this, RegionFullActivity.class).putExtra("screen", "7"));
                break;
        }
        return true;
    }
}